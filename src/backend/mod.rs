//! This module contains backend-agnostic traits and the implementation
//! of the `gfx-rs` backend.
pub mod gfx;

use std::path::Path;
use std::{fs, io};

use image::{ColorType, DynamicImage};

use style::{Color, Effect};
use {geometry, object, State};

/// An image handle.
pub trait Image: PartialEq + Clone {
    type Backend: Backend;

    /// Creates a new image from the provided data.
    fn new(backend: &mut Self::Backend, data: &DynamicImage) -> Self;

    /// Replaces image data with the provided one.
    fn replace(&mut self, backend: &mut Self::Backend, data: &DynamicImage);

    /// The color type of the allocated image on the GPU side.
    ///
    /// This value might not match the color type of the image data.
    fn color(&self) -> ColorType;

    /// Image dimensions, in pixels.
    fn dimensions(&self) -> (u16, u16);

    /// Image width, in pixels.
    fn width(&self) -> u16 {
        self.dimensions().0
    }

    /// Image height, in pixels.
    fn height(&self) -> u16 {
        self.dimensions().1
    }
}

/// A font handle.
pub trait Font: Clone {
    type Backend: Backend;

    /// Creates a new font from the raw truetype data. `index` is the position of the font
    /// in the collection, defaulting to the first available font.
    fn from_bytes(backend: &mut Self::Backend, bytes: Vec<u8>, index: Option<usize>) -> Self;

    /// Creates a new font, reading data from the file.
    fn from_file<P: AsRef<Path>>(
        backend: &mut Self::Backend,
        path: P,
        index: Option<usize>,
    ) -> io::Result<Self>
    where
        Self: Sized,
    {
        let buf = fs::read(path)?;
        Ok(Self::from_bytes(backend, buf, index))
    }
}

/// Rendering backend.
pub trait Backend: Sized {
    type Image: Image<Backend = Self>;
    type Font: Font<Backend = Self>;

    /// Flushes buffers and drawing the frame.
    ///
    /// This function is being called after all drawing operations.
    fn commit(&mut self);

    /// Updates the logical size of the window operating on.
    fn resize(&mut self, size: (f32, f32));

    /// Updates the logical size of the window operating on.
    fn set_hidpi_factor(&mut self, factor: f32);

    /// Returns current hidpi factor of the window operating on.
    fn get_hidpi_factor(&self) -> f32;

    /// Sets the scissor rectangle, discarding all pixels which are not contained in it.
    fn set_scissor(&mut self, rect: geometry::Rect);

    /// Clears the scissor rectangle.
    fn clear_scissor(&mut self);

    /// Sets the effect, applying to the subsequent operations.
    fn set_effect(&mut self, effect: Effect);

    /// Clears the entire frame with the specified color.
    fn clear(&mut self, color: Color);

    /// Schedules a rectangle to be drawn.
    fn rect(&mut self, state: State<Self>, rect: object::Rect);

    /// Schedules a circle to be drawn.
    fn circle(&mut self, state: State<Self>, circle: object::Circle);

    /// Schedules an ellipse to be drawn.
    fn ellipse(&mut self, state: State<Self>, ellipse: object::Ellipse);

    /// Schedules a path to be drawn.
    fn path(&mut self, state: State<Self>, path: object::Path);

    /// Schedules a text to be drawn.
    fn text(&mut self, state: State<Self>, text: object::Text);
}
