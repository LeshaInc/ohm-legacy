use gfx;
use gfx::format::{ChannelType, R8, Swizzle, U8Norm};
use gfx::handle::{DepthStencilView, RenderTargetView, Texture};
use gfx::memory::{Bind, Usage};
use gfx::preset::blend;
use gfx::state::ColorMask;
use gfx::texture::{AaMode, FilterMethod, ImageInfoCommon, Kind, SamplerInfo, WrapMode};
use gfx::{CommandBuffer, Encoder, Factory, Resources};

use euclid::approxeq::ApproxEq;

use rusttype::gpu_cache::Cache;
use rusttype::{self, PositionedGlyph};

use unicode_normalization::UnicodeNormalization;

use backend::gfx::{ColorFormat, Config, DepthFormat, GfxBackend, ShaderApi, ShaderSource};
use geometry::{Transform2D, Vector3D};
use object::Text;
use State;

use super::instancer::{Instancer, InstancerBuilder};

gfx_defines! {
    vertex Vertex {
        position: [f32; 2] = "inPos",
    }

    vertex Instance {
        transform_row0: [f32; 4] = "inTransform0",
        transform_row1: [f32; 4] = "inTransform1",
        transform_row2: [f32; 4] = "inTransform2",
        rect_min: [f32; 2] = "inRectMin",
        rect_max: [f32; 2] = "inRectMax",
        size: [f32; 2] = "inSize",
        z: f32 = "inZ",
        fill_color: [f32; 4] = "inFillColor",
    }

    pipeline pipe {
        vertices: gfx::VertexBuffer<Vertex> = (),
        instances: gfx::InstanceBuffer<Instance> = (),
        projection: gfx::Global<[[f32; 4]; 4]> = "uProj",
        alpha_multiplier: gfx::Global<f32> = "uAlphaMultiplier",
        cache: gfx::TextureSampler<f32> = "uCache",
        color_target: gfx::BlendTarget<ColorFormat> = ("outColor", ColorMask::all(), blend::ALPHA),
        depth_target: gfx::DepthTarget<DepthFormat> =
            gfx::preset::depth::LESS_EQUAL_WRITE,
        scissor: gfx::Scissor = (),
    }
}

impl ApproxEq<f32> for Instance {
    fn approx_epsilon() -> f32 {
        1.0e-6
    }

    fn approx_eq(&self, other: &Instance) -> bool {
        self.approx_eq_eps(other, &Instance::approx_epsilon())
    }

    fn approx_eq_eps(&self, other: &Instance, eps: &f32) -> bool {
        vec4_approx_eq(self.transform_row0, other.transform_row0, eps)
            && vec4_approx_eq(self.transform_row1, other.transform_row1, eps)
            && vec4_approx_eq(self.transform_row2, other.transform_row2, eps)
            && vec4_approx_eq(self.fill_color, other.fill_color, eps)
            && self.z.approx_eq(&other.z)
    }
}

fn vec4_approx_eq(a: [f32; 4], b: [f32; 4], eps: &f32) -> bool {
    a[0].approx_eq_eps(&b[0], eps)
        && a[1].approx_eq_eps(&b[1], eps)
        && a[2].approx_eq_eps(&b[2], eps)
        && a[3].approx_eq_eps(&b[3], eps)
}

struct Entry {
    glyph: PositionedGlyph<'static>,
    font_id: usize,
    color: [f32; 4],
    transform: Transform2D,
    z: f32,
}

pub struct Texts<R: Resources> {
    pub pipe: pipe::Data<R>,
    instancer: Instancer<R, pipe::Meta, Vertex, Instance>,
    cache: Cache<'static>,
    cache_texture: Texture<R, R8>,
    glyphs: Vec<Entry>,
    offset: usize,
    pub(crate) fonts: Vec<rusttype::Font<'static>>,
}

const RECT_VERTICES: &'static [Vertex] = &[
    Vertex {
        position: [0.0, 0.0],
    },
    Vertex {
        position: [1.0, 0.0],
    },
    Vertex {
        position: [1.0, 1.0],
    },
    Vertex {
        position: [0.0, 0.0],
    },
    Vertex {
        position: [1.0, 1.0],
    },
    Vertex {
        position: [0.0, 1.0],
    },
];

lazy_static! {
    static ref VERT_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../shader/glsl_150_core/text.vert")
    );
    static ref FRAG_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../shader/glsl_150_core/text.frag"),
    );
}

impl<R: Resources> Texts<R> {
    pub fn new<F: Factory<R>>(
        factory: &mut F,
        config: Config,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
    ) -> Texts<R> {
        let instancer = InstancerBuilder::<_, Vertex, Instance>::new()
            .with_capacity(config.initial_primitive_capacity)
            .with_pipeline(
                VERT_SHADER.select(config.api),
                FRAG_SHADER.select(config.api),
                pipe::new(),
            )
            .with_geometry(RECT_VERTICES, None)
            .build(factory);

        let (width, height) = (config.font_cache_width, config.font_cache_height);

        let cache = Cache::builder()
            .dimensions(width, height)
            .multithread(config.font_multithreaded)
            .scale_tolerance(config.font_cache_scale_tolerance)
            .position_tolerance(config.font_cache_position_tolerance)
            .pad_glyphs(true)
            .build();

        let kind = Kind::D2(width as u16, height as u16, AaMode::Single);

        let texture = factory
            .create_texture::<R8>(
                kind,
                1,
                Bind::SHADER_RESOURCE,
                Usage::Dynamic,
                Some(ChannelType::Unorm),
            )
            .unwrap();

        let sampler =
            factory.create_sampler(SamplerInfo::new(FilterMethod::Trilinear, WrapMode::Tile));

        let view = factory
            .view_texture_as_shader_resource::<U8Norm>(&texture, (0, 0), Swizzle::new())
            .unwrap();

        let pipe = pipe::Data {
            vertices: instancer.vertex_buffer.clone(),
            instances: instancer.instance_buffer.clone(),

            color_target,
            depth_target,

            projection: [[0.0; 4]; 4],
            alpha_multiplier: config.font_alpha_multiplier,
            cache: (view, sampler),

            scissor: gfx::Rect {
                x: 0,
                y: 0,
                w: 0,
                h: 0,
            },
        };

        Texts {
            pipe,
            instancer,
            cache,
            cache_texture: texture,
            glyphs: Vec::new(),
            fonts: Vec::new(),
            offset: 0,
        }
    }

    pub fn update_views(
        &mut self,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
    ) {
        self.pipe.color_target = color_target;
        self.pipe.depth_target = depth_target;
    }

    pub fn schedule<C, F>(&mut self, state: State<GfxBackend<R, F, C>>, z: f32, text: Text)
    where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        let font_id = state.font.id;
        let font = &self.fonts[state.font.id];

        let chars = text.string.chars().nfc();

        let scale = rusttype::Scale {
            x: text.scale_x,
            y: text.scale_y,
        };

        let start = rusttype::Vector {
            x: text.origin.x,
            y: text.origin.y,
        };

        let glyphs = font
            .glyphs_for(chars)
            .scan((None, 0.0), |(ref mut last, ref mut x), g| {
                let g = g.scaled(scale);
                if let Some(last) = *last {
                    *x += font.pair_kerning(scale, last, g.id());
                }
                let w = g.h_metrics().advance_width;
                let v = start + rusttype::vector(*x, 0.0);
                let next = g.positioned(rusttype::point(v.x, v.y));
                *last = Some(next.id());
                *x += w;
                Some(next)
            });

        for glyph in glyphs {
            let entry = Entry {
                font_id,
                z,
                glyph: glyph.standalone(),
                color: state.fill.color.into(),
                transform: state.transform,
            };

            self.glyphs.push(entry);
            self.cache.queue_glyph(font_id, glyph.standalone());
        }
    }

    pub fn draw<C, F>(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F)
    where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        let tex = self.cache_texture.clone();
        self.cache
            .cache_queued(|rect, data| {
                let info = ImageInfoCommon {
                    xoffset: rect.min.x as u16,
                    yoffset: rect.min.y as u16,
                    zoffset: 0,
                    width: rect.width() as u16,
                    height: rect.height() as u16,
                    depth: 0,
                    format: (),
                    mipmap: 0,
                };

                encoder
                    .update_texture::<R8, U8Norm>(&tex, None, info, data)
                    .unwrap();
            })
            .unwrap();

        for entry in self.glyphs.iter().skip(self.offset) {
            let (uv, rect) = match self.cache.rect_for(entry.font_id, &entry.glyph).unwrap() {
                Some(v) => v,
                None => continue,
            };

            let rows = entry
                .transform
                .to_3d()
                .pre_translate(Vector3D::new(rect.min.x as f32, rect.min.y as f32, 0.0))
                .to_column_arrays();

            let instance = Instance {
                fill_color: entry.color.into(),
                transform_row0: rows[0],
                transform_row1: rows[1],
                transform_row2: rows[2],
                size: [rect.width() as f32, rect.height() as f32],
                rect_min: [uv.min.x, uv.min.y],
                rect_max: [uv.max.x, uv.max.y],
                z: entry.z,
            };

            self.instancer.schedule(instance);
            self.offset += 1;
        }

        self.instancer.update(encoder, factory);
        self.pipe.vertices = self.instancer.vertex_buffer.clone();
        self.pipe.instances = self.instancer.instance_buffer.clone();
        self.instancer.draw(&self.pipe, encoder);
    }

    pub fn commit<C, F>(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F)
    where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        self.draw(encoder, factory);
        self.instancer.commit();

        self.glyphs.clear();
        self.offset = 0;
    }
}
