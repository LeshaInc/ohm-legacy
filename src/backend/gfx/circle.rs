use std::f32::consts::PI;

use gfx;
use gfx::handle::{DepthStencilView, RenderTargetView, Sampler, ShaderResourceView};
use gfx::preset::blend;
use gfx::state::ColorMask;
use gfx::{CommandBuffer, Encoder, Factory, Resources};

use euclid::approxeq::ApproxEq;

use backend::gfx::{ColorFormat, DepthFormat, ShaderApi, ShaderSource};
use geometry::Vector3D;
use object::{Circle, Ellipse};
use State;

use super::instancer::{Instancer, InstancerBuilder};
use super::{Config, GfxBackend};

gfx_defines! {
    vertex Vertex {
        position: [f32; 2] = "inPos",
        uv: [f32; 2] = "inUV",
    }

    vertex Instance {
        transform_row0: [f32; 4] = "inTransform0",
        transform_row1: [f32; 4] = "inTransform1",
        transform_row2: [f32; 4] = "inTransform2",
        size: [f32; 2] = "inSize",
        z: f32 = "inZ",
        fill_color: [f32; 4] = "inFillColor",
    }

    pipeline pipe {
        vertices: gfx::VertexBuffer<Vertex> = (),
        instances: gfx::InstanceBuffer<Instance> = (),
        projection: gfx::Global<[[f32; 4]; 4]> = "uProj",
        texture: gfx::TextureSampler<[f32; 4]> = "uTexture",
        color_target: gfx::BlendTarget<ColorFormat> = ("outColor", ColorMask::all(), blend::ALPHA),
        depth_target: gfx::DepthTarget<DepthFormat> =
            gfx::preset::depth::LESS_EQUAL_WRITE,
        scissor: gfx::Scissor = (),
    }
}

impl ApproxEq<f32> for Instance {
    fn approx_epsilon() -> f32 {
        1.0e-6
    }

    fn approx_eq(&self, other: &Instance) -> bool {
        self.approx_eq_eps(other, &Instance::approx_epsilon())
    }

    fn approx_eq_eps(&self, other: &Instance, eps: &f32) -> bool {
        vec4_approx_eq(self.transform_row0, other.transform_row0, eps)
            && vec4_approx_eq(self.transform_row1, other.transform_row1, eps)
            && vec4_approx_eq(self.transform_row2, other.transform_row2, eps)
            && vec4_approx_eq(self.fill_color, other.fill_color, eps)
            && self.z.approx_eq(&other.z)
    }
}

fn vec4_approx_eq(a: [f32; 4], b: [f32; 4], eps: &f32) -> bool {
    a[0].approx_eq_eps(&b[0], eps)
        && a[1].approx_eq_eps(&b[1], eps)
        && a[2].approx_eq_eps(&b[2], eps)
        && a[3].approx_eq_eps(&b[3], eps)
}

pub struct Circles<R: Resources> {
    pub pipe: pipe::Data<R>,
    instancer: Instancer<R, pipe::Meta, Vertex, Instance>,
}

const NUM_VERTICES: usize = 256;

fn vert(x: f32, y: f32) -> Vertex {
    Vertex {
        position: [x, y],
        uv: [(x + 1.0) / 2.0, (y + 1.0) / 2.0],
    }
}

lazy_static! {
    static ref CIRCLE: (Vec<Vertex>, Vec<u16>) = {
        let mut vertices = Vec::with_capacity(NUM_VERTICES);
        let mut indices = Vec::with_capacity(NUM_VERTICES * 3);

        vertices.push(vert(0.0, 0.0));

        let step = 2.0 * PI / (NUM_VERTICES as f32);
        let mut cur = 0.0f32;

        for i in 0..NUM_VERTICES {
            vertices.push(vert(cur.cos(), cur.sin()));

            indices.push((vertices.len() - 1) as u16);

            if i + 1 == NUM_VERTICES {
                indices.push(1)
            } else {
                indices.push(vertices.len() as u16);
            }

            indices.push(0);

            cur += step;
        }

        (vertices, indices)
    };
}

lazy_static! {
    static ref VERT_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../shader/glsl_150_core/circle.vert")
    );
    static ref FRAG_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../shader/glsl_150_core/circle.frag"),
    );
}

impl<R: Resources> Circles<R> {
    pub fn new<F: Factory<R>>(
        factory: &mut F,
        config: Config,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
        texture: ShaderResourceView<R, [f32; 4]>,
        sampler: Sampler<R>,
    ) -> Circles<R> {
        let instancer = InstancerBuilder::<_, Vertex, Instance>::new()
            .with_capacity(config.initial_primitive_capacity)
            .with_pipeline(
                VERT_SHADER.select(config.api),
                FRAG_SHADER.select(config.api),
                pipe::new(),
            )
            .with_geometry(&CIRCLE.0, Some(&CIRCLE.1))
            .build(factory);

        let pipe = pipe::Data {
            vertices: instancer.vertex_buffer.clone(),
            instances: instancer.instance_buffer.clone(),

            color_target,
            depth_target,

            projection: [[0.0; 4]; 4],
            texture: (texture, sampler),

            scissor: gfx::Rect {
                x: 0,
                y: 0,
                w: 0,
                h: 0,
            },
        };

        Circles { pipe, instancer }
    }

    pub fn update_views(
        &mut self,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
    ) {
        self.pipe.color_target = color_target;
        self.pipe.depth_target = depth_target;
    }

    pub fn schedule_circle<C, F>(
        &mut self,
        state: State<GfxBackend<R, F, C>>,
        z: f32,
        circle: Circle,
    ) where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        self.schedule_ellipse(
            state,
            z,
            Ellipse {
                origin: circle.origin,
                radius_x: circle.radius,
                radius_y: circle.radius,
            },
        );
    }

    pub fn schedule_ellipse<C, F>(
        &mut self,
        state: State<GfxBackend<R, F, C>>,
        z: f32,
        ellipse: Ellipse,
    ) where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        let rows = state
            .transform
            .to_3d()
            .pre_translate(Vector3D::new(ellipse.origin.x, ellipse.origin.y, 0.0))
            .to_column_arrays();

        let instance = Instance {
            fill_color: state.fill.color.into(),
            transform_row0: rows[0],
            transform_row1: rows[1],
            transform_row2: rows[2],
            size: [ellipse.radius_x, ellipse.radius_y],
            z: z,
        };

        self.instancer.schedule(instance);
    }

    pub fn draw<C, F>(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F)
    where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        self.instancer.update(encoder, factory);
        self.pipe.vertices = self.instancer.vertex_buffer.clone();
        self.pipe.instances = self.instancer.instance_buffer.clone();
        self.instancer.draw(&self.pipe, encoder);
    }

    pub fn commit<C, F>(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F)
    where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        self.draw(encoder, factory);
        self.instancer.commit();
    }
}
