use gfx;
use gfx::handle::{DepthStencilView, RenderTargetView, Sampler, ShaderResourceView};
use gfx::preset::blend;
use gfx::state::ColorMask;
use gfx::{CommandBuffer, Encoder, Factory, Resources};

use euclid::approxeq::ApproxEq;

use super::batcher::{Batcher, BatcherBuilder};
use super::{ColorFormat, Config, DepthFormat, GfxBackend, ShaderApi, ShaderSource};
use object::{Path, TessellatedPath};
use State;

gfx_defines! {
    vertex Vertex {
        position: [f32; 2] = "inPos",
        uv: [f32; 2] = "inUV",
        color: [f32; 4] = "inColor",
        z: f32 = "inZ",
    }

    pipeline pipe {
        vertices: gfx::VertexBuffer<Vertex> = (),
        projection: gfx::Global<[[f32; 4]; 4]> = "uProj",
        texture: gfx::TextureSampler<[f32; 4]> = "uTexture",
        color_target: gfx::BlendTarget<ColorFormat> = ("outColor", ColorMask::all(), blend::ALPHA),
        depth_target: gfx::DepthTarget<DepthFormat> =
            gfx::preset::depth::LESS_EQUAL_WRITE,
        scissor: gfx::Scissor = (),
    }
}

lazy_static! {
    static ref VERT_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../shader/glsl_150_core/path.vert")
    );
    static ref FRAG_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../shader/glsl_150_core/path.frag"),
    );
}

struct Entry<R, F, C>
where
    R: Resources,
    C: CommandBuffer<R>,
    F: Factory<R>,
{
    state: State<GfxBackend<R, F, C>>,
    z: f32,
    path: Path,
    tessellated: TessellatedPath,
}

pub struct Paths<R, F, C>
where
    R: Resources,
    C: CommandBuffer<R>,
    F: Factory<R>,
{
    pub pipe: pipe::Data<R>,
    batcher: Batcher<R, pipe::Meta, Vertex>,
    scheduled: Vec<Entry<R, F, C>>,
    pos: usize,
    start: usize,
    clear_texture: (ShaderResourceView<R, [f32; 4]>, Sampler<R>),
}

impl<R, F, C> Paths<R, F, C>
where
    R: Resources,
    C: CommandBuffer<R>,
    F: Factory<R>,
{
    pub fn new(
        factory: &mut F,
        config: Config,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
        texture: ShaderResourceView<R, [f32; 4]>,
        sampler: Sampler<R>,
    ) -> Paths<R, F, C> {
        let batcher = BatcherBuilder::<_, Vertex>::new()
            .with_vbuf_capacity(config.initial_vbuf_capacity)
            .with_ibuf_capacity(config.initial_ibuf_capacity)
            .with_pipeline(
                VERT_SHADER.select(config.api),
                FRAG_SHADER.select(config.api),
                pipe::new(),
            )
            .build(factory);

        let pipe = pipe::Data {
            vertices: batcher.vertex_buffer.clone(),

            color_target,
            depth_target,

            projection: [[0.0; 4]; 4],
            texture: (texture, sampler),
            scissor: gfx::Rect {
                x: 0,
                y: 0,
                w: 0,
                h: 0,
            },
        };

        Paths {
            clear_texture: pipe.texture.clone(),
            pipe,
            batcher,
            scheduled: Vec::new(),
            pos: 0,
            start: 0,
        }
    }

    pub fn update_views(
        &mut self,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
    ) {
        self.pipe.color_target = color_target;
        self.pipe.depth_target = depth_target;
    }

    fn should_keep(&mut self, state: &State<GfxBackend<R, F, C>>, z: f32, path: &Path) -> bool {
        let old = match self.scheduled.get(self.pos) {
            Some(v) => v,
            None => return false,
        };

        let should_keep = old.state.has_fill() == state.has_fill()
            && old.state.has_stroke() == state.has_stroke()
            && old
                .state
                .stroke
                .thickness
                .approx_eq(&state.stroke.thickness) && old.z.approx_eq(&z)
            && old.path.approx_eq(path);

        should_keep
    }

    pub(crate) fn schedule(&mut self, state: State<GfxBackend<R, F, C>>, z: f32, path: Path) {
        let should_keep = self.should_keep(&state, z, &path);

        if !should_keep {
            let existed_before = self.scheduled.len() > self.pos;

            let tessellated = path.tessellate(state.fill.clone(), state.stroke.clone());

            let entry = Entry {
                state,
                z,
                tessellated,
                path,
            };

            if existed_before {
                self.scheduled[self.pos] = entry;
            } else {
                self.scheduled.push(entry);
            }
        } else {
            self.scheduled[self.pos].state = state;
        }

        self.pos += 1;
    }

    pub fn draw(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F) {
        for entry in &self.scheduled[self.start..self.pos] {
            if let Some(ref path) = entry.tessellated.fill {
                self.batcher.next_mesh();

                for v in &path.vertices {
                    self.batcher.add_vertex(Vertex {
                        position: entry
                            .state
                            .transform
                            .transform_point(&v.position.into())
                            .into(),
                        uv: v.uv,
                        color: entry.state.fill.color.into(),
                        z: entry.z,
                    });
                }

                for &i in &path.indices {
                    self.batcher.add_index(i);
                }
            }

            if let Some(ref path) = entry.tessellated.stroke {
                let old_tex = self.pipe.texture.clone();

                if old_tex != self.clear_texture {
                    self.batcher.update(encoder, factory);
                    self.pipe.vertices = self.batcher.vertex_buffer.clone();
                    self.batcher.draw(&self.pipe, encoder);
                    self.pipe.texture = self.clear_texture.clone();
                }

                self.batcher.next_mesh();

                for v in &path.vertices {
                    self.batcher.add_vertex(Vertex {
                        position: entry
                            .state
                            .transform
                            .transform_point(&v.position.into())
                            .into(),
                        uv: v.uv,
                        color: entry.state.stroke.color.into(),
                        z: entry.z,
                    });
                }

                for &i in &path.indices {
                    self.batcher.add_index(i);
                }

                if old_tex != self.clear_texture {
                    self.batcher.update(encoder, factory);
                    self.pipe.vertices = self.batcher.vertex_buffer.clone();
                    self.batcher.draw(&self.pipe, encoder);
                }

                self.pipe.texture = old_tex;
            }
        }

        self.start = self.pos;

        self.batcher.update(encoder, factory);
        self.pipe.vertices = self.batcher.vertex_buffer.clone();
        self.batcher.draw(&self.pipe, encoder);
    }

    pub fn commit(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F) {
        self.draw(encoder, factory);
        self.batcher.commit();

        self.pos = 0;
        self.start = 0;
    }
}
