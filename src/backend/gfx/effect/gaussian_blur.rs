use gfx;
use gfx::handle::{Buffer, DepthStencilView, RenderTargetView, Sampler, ShaderResourceView};
use gfx::preset::blend;
use gfx::state::ColorMask;
use gfx::traits::FactoryExt;
use gfx::{CommandBuffer, Encoder, Factory, PipelineState, Resources, Slice};

use backend::gfx::{ColorFormat, Config, DepthFormat, ShaderApi, ShaderSource};

use super::{create_targets, Vertex, VERT_SHADER};

gfx_defines! {
    pipeline pipe {
        vertices: gfx::VertexBuffer<Vertex> = (),
        input: gfx::TextureSampler<[f32; 4]> = "uInput",
        direction: gfx::Global<[f32; 2]> = "uDirection",
        resolution: gfx::Global<[f32; 2]> = "uResolution",
        z: gfx::Global<f32> = "uZ",
        color_target: gfx::BlendTarget<ColorFormat> = ("outColor", ColorMask::all(), blend::ALPHA),
        depth_target: gfx::DepthTarget<DepthFormat> =
            gfx::preset::depth::LESS_EQUAL_WRITE,
    }
}

lazy_static! {
    static ref FRAG_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../../shader/glsl_150_core/gaussian_blur.frag")
    );
}

pub struct GaussianBlur<R: Resources> {
    pso: PipelineState<R, pipe::Meta>,
    inter_tex: (ShaderResourceView<R, [f32; 4]>, Sampler<R>),
    inter_rtv: RenderTargetView<R, ColorFormat>,
    inter_dsv: DepthStencilView<R, DepthFormat>,

    size: (f32, f32),
}

impl<R: Resources> GaussianBlur<R> {
    pub fn new<F: Factory<R>>(factory: &mut F, config: Config, dim: (u16, u16)) -> GaussianBlur<R> {
        let pso = factory
            .create_pipeline_simple(
                VERT_SHADER.select(config.api),
                FRAG_SHADER.select(config.api),
                pipe::new(),
            )
            .unwrap();

        let (inter_tex, inter_rtv, inter_dsv) = create_targets(factory, dim);

        GaussianBlur {
            pso,
            inter_tex,
            inter_rtv,
            inter_dsv,
            size: (dim.0 as f32, dim.1 as f32)
        }
    }

    pub fn resize<F: Factory<R>>(&mut self, factory: &mut F, dim: (u16, u16), fac: f32) {
        let logical = (dim.0 as f32 / fac, dim.1 as f32 / fac);
        self.size = logical;

        let (inter_tex, inter_rtv, inter_dsv) = create_targets(factory, dim);
        self.inter_tex = inter_tex;
        self.inter_rtv = inter_rtv;
        self.inter_dsv = inter_dsv;
    }

    pub fn draw<C: CommandBuffer<R>>(
        &self,
        encoder: &mut Encoder<R, C>,
        vbuf: Buffer<R, Vertex>,
        slice: &Slice<R>,
        input: (ShaderResourceView<R, [f32; 4]>, Sampler<R>),
        color_target: &RenderTargetView<R, ColorFormat>,
        depth_target: &DepthStencilView<R, DepthFormat>,
        z: f32,
        amount: f32,
    ) {
        encoder.clear(&self.inter_rtv, [0.0, 0.0, 0.0, 0.0]);
        encoder.clear_depth(&self.inter_dsv, 1.0);

        let amount = amount / 9.0;

        let mut data = pipe::Data {
            input,
            z,
            vertices: vbuf,
            direction: [amount, 0.0],
            resolution: [self.size.0, self.size.1],
            color_target: self.inter_rtv.clone(),
            depth_target: self.inter_dsv.clone(),
        };

        encoder.draw(slice, &self.pso, &data);

        data.direction = [0.0, amount];
        data.input = self.inter_tex.clone();
        data.color_target = color_target.clone();
        data.depth_target = depth_target.clone();

        encoder.draw(slice, &self.pso, &data);
    }
}
