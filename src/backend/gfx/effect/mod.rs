mod gaussian_blur;

use gfx::format::{ChannelType, Swizzle};
use gfx::handle::{Buffer, DepthStencilView, RenderTargetView, Sampler, ShaderResourceView};
use gfx::memory::{Bind, Usage};
use gfx::texture::{AaMode, DepthStencilFlags, FilterMethod, Kind, SamplerInfo, WrapMode};
use gfx::traits::FactoryExt;
use gfx::{CommandBuffer, Encoder, Factory, Resources, Slice};

use style::Effect;

use backend::gfx::{ColorFormat, Config, DepthFormat, ShaderApi, ShaderSource};

use self::gaussian_blur::GaussianBlur;

gfx_defines! {
    vertex Vertex {
        position: [f32; 2] = "inPos",
        uv: [f32; 2] = "inUV",
    }
}

pub const RECT_VERTICES: &'static [Vertex] = &[
    Vertex {
        position: [-1.0, -1.0],
        uv: [0.0, 0.0],
    },
    Vertex {
        position: [1.0, -1.0],
        uv: [1.0, 0.0],
    },
    Vertex {
        position: [1.0, 1.0],
        uv: [1.0, 1.0],
    },
    Vertex {
        position: [-1.0, -1.0],
        uv: [0.0, 0.0],
    },
    Vertex {
        position: [1.0, 1.0],
        uv: [1.0, 1.0],
    },
    Vertex {
        position: [-1.0, 1.0],
        uv: [0.0, 1.0],
    },
];

lazy_static! {
    pub static ref VERT_SHADER: ShaderSource = ShaderSource::new().with(
        ShaderApi::Glsl(150),
        include_bytes!("../../../../shader/glsl_150_core/effect.vert")
    );
}

pub fn create_targets<R: Resources, F: Factory<R>>(
    factory: &mut F,
    (width, height): (u16, u16),
) -> (
    (ShaderResourceView<R, [f32; 4]>, Sampler<R>),
    RenderTargetView<R, ColorFormat>,
    DepthStencilView<R, DepthFormat>,
) {
    let kind = Kind::D2(width, height, AaMode::Single);

    let rt_tex = factory
        .create_texture(
            kind,
            1,
            Bind::RENDER_TARGET | Bind::SHADER_RESOURCE,
            Usage::Dynamic,
            Some(ChannelType::Srgb),
        )
        .unwrap();

    let ds_tex = factory
        .create_texture(
            kind,
            1,
            Bind::DEPTH_STENCIL,
            Usage::Dynamic,
            Some(ChannelType::Unorm),
        )
        .unwrap();

    let sampler = factory.create_sampler(SamplerInfo::new(FilterMethod::Bilinear, WrapMode::Tile));

    let srv = factory
        .view_texture_as_shader_resource::<ColorFormat>(&rt_tex, (0, 0), Swizzle::new())
        .unwrap();

    let rtv = factory
        .view_texture_as_render_target(&rt_tex, 0, None)
        .unwrap();

    let dsv = factory
        .view_texture_as_depth_stencil(&ds_tex, 0, None, DepthStencilFlags::empty())
        .unwrap();

    ((srv, sampler), rtv, dsv)
}

pub struct Effects<R: Resources> {
    pub effect: Effect,

    config: Config,

    vbuf: Buffer<R, Vertex>,
    slice: Slice<R>,

    entry_tex: (ShaderResourceView<R, [f32; 4]>, Sampler<R>),
    pub entry_rtv: RenderTargetView<R, ColorFormat>,
    pub entry_dsv: DepthStencilView<R, DepthFormat>,

    gaussian_blur: GaussianBlur<R>,
}

impl<R: Resources> Effects<R> {
    pub fn new<F: Factory<R>>(factory: &mut F, config: Config, dim: (u16, u16)) -> Effects<R> {
        let (entry_tex, entry_rtv, entry_dsv) = create_targets(factory, dim);

        let (vbuf, slice) = factory.create_vertex_buffer_with_slice(RECT_VERTICES, ());

        Effects {
            gaussian_blur: GaussianBlur::new(factory, config, dim),
            effect: Effect::Identity,
            vbuf,
            config,
            slice,
            entry_tex,
            entry_rtv,
            entry_dsv,
        }
    }

    pub fn clear<C: CommandBuffer<R>>(&self, encoder: &mut Encoder<R, C>) {
        encoder.clear(&self.entry_rtv, [0.0, 0.0, 0.0, 0.0]);
        encoder.clear_depth(&self.entry_dsv, 1.0);
    }

    pub fn resize<F: Factory<R>>(&mut self, factory: &mut F, dim: (u16, u16), fac: f32) {
        let (entry_tex, entry_rtv, entry_dsv) = create_targets(factory, dim);
        self.entry_tex = entry_tex;
        self.entry_rtv = entry_rtv;
        self.entry_dsv = entry_dsv;

        self.gaussian_blur.resize(factory, dim, fac);
    }

    pub fn draw<C: CommandBuffer<R>>(
        &self,
        encoder: &mut Encoder<R, C>,
        color_target: &RenderTargetView<R, ColorFormat>,
        depth_target: &DepthStencilView<R, DepthFormat>,
        z: f32,
    ) {
        let z = 1.0 - z / self.config.z_limit;

        match self.effect {
            Effect::GaussianBlur(a) => self.gaussian_blur.draw(
                encoder,
                self.vbuf.clone(),
                &self.slice,
                self.entry_tex.clone(),
                color_target,
                depth_target,
                z,
                a,
            ),
            _ => {}
        }
    }
}
