use std::marker::PhantomData;

use gfx::format::Rgba8;
use gfx::handle::{Sampler, ShaderResourceView};
use gfx::texture::{AaMode, FilterMethod, Kind, Mipmap, SamplerInfo, WrapMode};
use gfx::{CommandBuffer, Factory, Resources};

use image::{ColorType, DynamicImage, GenericImageView};

use backend;

use super::GfxBackend;

/// `gfx-rs` backend image handle.
#[derive(Debug)]
pub struct Image<R: Resources, F: Factory<R>, C: CommandBuffer<R>> {
    pub(crate) view: ShaderResourceView<R, [f32; 4]>,
    pub(crate) sampler: Sampler<R>,
    dimensions: (u16, u16),
    _marker: PhantomData<(F, C)>,
}

impl<R: Resources, F: Factory<R>, C: CommandBuffer<R>> PartialEq for Image<R, F, C> {
    fn eq(&self, other: &Image<R, F, C>) -> bool {
        self.view == other.view
            && self.sampler == other.sampler
            && self.dimensions == other.dimensions
    }
}

impl<R: Resources, F: Factory<R>, C: CommandBuffer<R>> Eq for Image<R, F, C> {}

impl<R: Resources, F: Factory<R>, C: CommandBuffer<R>> Clone for Image<R, F, C> {
    fn clone(&self) -> Image<R, F, C> {
        Image {
            view: self.view.clone(),
            sampler: self.sampler.clone(),
            dimensions: self.dimensions,
            _marker: Default::default(),
        }
    }
}

fn create_texture<R, F, C>(
    back: &mut GfxBackend<R, F, C>,
    data: &DynamicImage,
) -> (ShaderResourceView<R, [f32; 4]>, Sampler<R>)
where
    R: Resources,
    F: Factory<R>,
    C: CommandBuffer<R>,
{
    let im = data.to_rgba();
    let kind = Kind::D2(data.width() as u16, data.height() as u16, AaMode::Single);
    let mipmap = Mipmap::Provided;

    let (_, view) = back
        .factory
        .create_texture_immutable_u8::<Rgba8>(kind, mipmap, &[&im])
        .unwrap();

    let sampler = back
        .factory
        .create_sampler(SamplerInfo::new(FilterMethod::Trilinear, WrapMode::Tile));

    (view, sampler)
}

impl<R: Resources, F: Factory<R>, C: CommandBuffer<R>> backend::Image for Image<R, F, C> {
    type Backend = GfxBackend<R, F, C>;

    fn new(backend: &mut Self::Backend, data: &DynamicImage) -> Image<R, F, C> {
        let (view, sampler) = create_texture(backend, data);

        Image {
            view,
            sampler,
            dimensions: (data.width() as u16, data.height() as u16),
            _marker: Default::default(),
        }
    }

    fn replace(&mut self, backend: &mut Self::Backend, data: &DynamicImage) {
        let (view, sampler) = create_texture(backend, data);
        self.view = view;
        self.sampler = sampler;
        self.dimensions = (data.width() as u16, data.height() as u16);
    }

    fn color(&self) -> ColorType {
        ColorType::RGBA(8)
    }

    fn dimensions(&self) -> (u16, u16) {
        self.dimensions
    }
}
