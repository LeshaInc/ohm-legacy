use super::ShaderApi;

/// Global `gfx-rs` backend configuration.
///
/// # Example
///
/// ```rust,ignore
/// let config = Config::new()
///     .initial_primitive_capacity(2048)
///     .font_cache_dimensions(1024, 1024)
///     .font_multithreaded(true);
/// ```
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Config {
    pub(crate) initial_primitive_capacity: usize,
    pub(crate) initial_vbuf_capacity: usize,
    pub(crate) initial_ibuf_capacity: usize,
    pub(crate) font_cache_width: u32,
    pub(crate) font_cache_height: u32,
    pub(crate) font_cache_scale_tolerance: f32,
    pub(crate) font_cache_position_tolerance: f32,
    pub(crate) font_multithreaded: bool,
    pub(crate) font_alpha_multiplier: f32,
    pub(crate) z_limit: f32,
    pub(crate) api: ShaderApi,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            initial_primitive_capacity: 512,
            initial_vbuf_capacity: 4096,
            initial_ibuf_capacity: 8192,
            font_cache_width: 1024,
            font_cache_height: 1024,
            font_cache_scale_tolerance: 0.1,
            font_cache_position_tolerance: 0.1,
            font_multithreaded: true,
            font_alpha_multiplier: 1.5,
            z_limit: 8_000_000.0,
            api: ShaderApi::Glsl(150),
        }
    }
}

impl Config {
    /// Creates a new config, filled with default values.
    ///
    /// This is equivalent to calling `Default::default()`.
    pub fn new() -> Config {
        Default::default()
    }

    /// Specifies initial primitive capacity, relevant for rectangles, circles, ellipses,
    /// and text (characters are being rendered using textured rectangles).
    ///
    /// For example, if the `capacity` is set to 1000, you would be able to render 1000
    /// rectangles without allocating. This way, drawing 1001st rectangle would cause
    /// reallocating the vector and the buffer on the GPU side.
    ///
    /// _Default value:_ `512`.
    pub fn initial_primitive_capacity(mut self, capacity: usize) -> Config {
        self.initial_primitive_capacity = capacity;
        self
    }

    /// Specifies initial vertex buffer capacity, relevant for paths.
    ///
    /// For example, if the `capacity` is set to 1000, you would be able to schedule 1000
    /// vertices without allocating. This way, adding a new 1001st vertex would cause
    /// reallocating the vector and the buffer on the GPU side.
    ///
    /// Keep in mind that primitives, e.g. rectangles, use paths for stroke
    /// rendering.
    ///
    /// _Default value:_ `4096`.
    pub fn initial_vbuf_capacity(mut self, capacity: usize) -> Config {
        self.initial_vbuf_capacity = capacity;
        self
    }

    /// Specifies initial index buffer capacity, relevant for paths.
    ///
    /// For example, if the `capacity` is set to 1000, you would be able to schedule 1000
    /// indices without allocating. This way, adding a new 1001st index would cause
    /// reallocating the vector and the buffer on the GPU side.
    ///
    /// Keep in mind that primitives, e.g. rectangles, use paths for stroke
    /// rendering.
    ///
    /// _Default value:_ `8192`.
    pub fn initial_ibuf_capacity(mut self, capacity: usize) -> Config {
        self.initial_ibuf_capacity = capacity;
        self
    }

    /// Specifies the font cache texture dimensions (width and height).
    ///
    /// The bigger cache is, the less GPU texture uploads need to be performed
    ///
    /// The cache would not grow.
    ///
    /// _Default value:_ `(1024, 1024)`.
    pub fn font_cache_dimensions(mut self, width: u32, height: u32) -> Config {
        self.font_cache_width = width;
        self.font_cache_height = height;
        self
    }

    /// Specifies the tolerance difference for judging whether an existing glyph
    /// ih the GPU cache is close enough to the requested glyph to be used
    /// in its place.
    ///
    /// Check rusttype documentation for more info.
    ///
    /// _Default value:_ `(0.1, 0.1)`.
    pub fn font_cache_tolerance(mut self, scale: f32, position: f32) -> Config {
        self.font_cache_scale_tolerance = scale;
        self.font_cache_position_tolerance = position;
        self
    }

    /// Specifies whether multiple CPU cores should be available for
    /// font rasterizing.
    ///
    /// _Default value:_ `true`.
    pub fn font_multithreaded(mut self, enabled: bool) -> Config {
        self.font_multithreaded = enabled;
        self
    }

    /// Specifies the alpha channel multiplier for text rendering.
    ///
    /// Rusttype currently does not support font hinting and produce blury and gray
    /// characters. Setting this value to `1.0` will draw text as is.
    ///
    /// Example (the text is fully black, e.g. `#000000`):
    ///
    /// ![example](https://gitlab.com/LeshaInc/ohm/raw/master/img/alpha_multiplier_example.png)
    ///
    /// _Default value:_ `1.5`.
    pub fn font_alpha_multiplier(mut self, multiplier: f32) -> Config {
        self.font_alpha_multiplier = multiplier;
        self
    }

    /// Specifies the z limit (far plane) for the projection.
    ///
    /// _Default value:_ `8_000_000.0`.
    pub fn z_limit(mut self, limit: f32) -> Config {
        self.z_limit = limit;
        self
    }

    /// Specifies the shader api for the backend.
    ///
    /// _Default value:_ `ShaderApi::Glsl(150)`
    pub fn api(mut self, api: ShaderApi) -> Config {
        self.api = api;
        self
    }
}
