//! `gfx-rs` rendering backend.
//!
//! # Example
//!
//! ```rust,ignore
//! let backend = GfxBackend::new(
//!     (800.0, 600.0),
//!     window.get_hidpi_factor() as f32,
//!     Default::default(),
//!     factory,
//!     encoder,
//!     color_target.clone(),
//!     depth_target.clone(),
//! );
//! ```
mod batcher;
mod circle;
mod config;
mod effect;
mod font;
mod image;
mod instancer;
mod path;
mod rect;
mod shader;
mod text;

use std::f32::consts::PI;

use gfx;
use gfx::format::Rgba8;
use gfx::handle::{DepthStencilView, RenderTargetView, Sampler, ShaderResourceView};
use gfx::texture::{AaMode, FilterMethod, Kind, Mipmap, SamplerInfo, WrapMode};
use gfx::Rect as ScissorRect;
use gfx::{CommandBuffer, Encoder, Factory, Resources};

use euclid::approxeq::ApproxEq;

use backend::Backend;
use geometry::{Rect, Transform3D, Vector2D, Vector3D};
use object::{Circle, Ellipse, Path, Text};
use style::{Color, Effect};
use State;

use self::circle::Circles;
use self::effect::Effects;
use self::path::Paths;
use self::rect::Rectangles;
use self::text::Texts;

pub use self::config::Config;
pub use self::font::Font;
pub use self::image::Image;
pub use self::shader::{ShaderApi, ShaderSource};

pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum ObjectType {
    Rect,
    Circle,
    Path,
    Text,
    None,
}

/// `GfxBackend` builder.
pub struct GfxBackendBuilder<R: Resources, F: Factory<R>, C: CommandBuffer<R>> {
    factory: Option<F>,
    encoder: Option<Encoder<R, C>>,
    color_target: Option<RenderTargetView<R, ColorFormat>>,
    depth_target: Option<DepthStencilView<R, DepthFormat>>,
    dimensions: (f32, f32),
    hidpi_factor: f32,
    config: Config,
}

impl<R: Resources, F: Factory<R>, C: CommandBuffer<R>> GfxBackendBuilder<R, F, C> {
    /// Creates a new builder with default values.
    pub fn new() -> GfxBackendBuilder<R, F, C> {
        GfxBackendBuilder {
            factory: None,
            encoder: None,
            color_target: None,
            depth_target: None,
            dimensions: (0.0, 0.0),
            hidpi_factor: 1.0,
            config: Default::default(),
        }
    }

    /// Specifies the `gfx-rs` factory.
    pub fn with_factory(mut self, factory: F) -> GfxBackendBuilder<R, F, C> {
        self.factory = Some(factory);
        self
    }

    /// Specifies the `gfx-rs` commands encoder.
    pub fn with_encoder(mut self, encoder: Encoder<R, C>) -> GfxBackendBuilder<R, F, C> {
        self.encoder = Some(encoder);
        self
    }

    /// Specifies color and depth target of the window.
    pub fn with_targets(
        mut self,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
    ) -> GfxBackendBuilder<R, F, C> {
        self.color_target = Some(color_target);
        self.depth_target = Some(depth_target);
        self
    }

    /// Specifies the logical dimensions of the window.
    pub fn with_dimensions<D: Into<(f32, f32)>>(
        mut self,
        dimensions: D,
    ) -> GfxBackendBuilder<R, F, C> {
        self.dimensions = dimensions.into();
        self
    }

    /// Specifies the hidpi factor of the window.
    pub fn with_hidpi_factor(mut self, factor: f32) -> GfxBackendBuilder<R, F, C> {
        self.hidpi_factor = factor;
        self
    }

    /// Specifies the backend config.
    pub fn with_config(mut self, config: Config) -> GfxBackendBuilder<R, F, C> {
        self.config = config;
        self
    }

    /// Builds the backend instance.
    pub fn build(self) -> GfxBackend<R, F, C> {
        let mut factory = self.factory.expect("factory is not provided");
        let encoder = self.encoder.expect("factory is not provided");
        let color_target = self.color_target.expect("color target is not provided");
        let depth_target = self.depth_target.expect("depth target is not provided");
        let size = self.dimensions;
        let hidpi_factor = self.hidpi_factor;
        let config = self.config;

        let kind = Kind::D2(1, 1, AaMode::Single);
        let mipmap = Mipmap::Provided;

        let (_, clear_texture) = factory
            .create_texture_immutable_u8::<Rgba8>(kind, mipmap, &[&[255, 255, 255, 255]])
            .unwrap();

        let clear_sampler =
            factory.create_sampler(SamplerInfo::new(FilterMethod::Scale, WrapMode::Tile));

        let rectangles = Rectangles::new(
            &mut factory,
            config,
            color_target.clone(),
            depth_target.clone(),
            clear_texture.clone(),
            clear_sampler.clone(),
        );

        let circles = Circles::new(
            &mut factory,
            config,
            color_target.clone(),
            depth_target.clone(),
            clear_texture.clone(),
            clear_sampler.clone(),
        );

        let paths = Paths::new(
            &mut factory,
            config,
            color_target.clone(),
            depth_target.clone(),
            clear_texture.clone(),
            clear_sampler.clone(),
        );

        let texts = Texts::new(
            &mut factory,
            config,
            color_target.clone(),
            depth_target.clone(),
        );

        let (width, height, ..) = color_target.get_dimensions();

        let effects = Effects::new(&mut factory, config, (width, height));

        GfxBackend {
            circles,
            color_target,
            config,
            depth_target,
            encoder,
            factory,
            paths,
            rectangles,
            texts,
            effects,
            projection: projection(size, config.z_limit),
            size: size,
            hidpi_factor: hidpi_factor,

            scissor: ScissorRect {
                x: 0,
                y: 0,
                w: (size.0 * hidpi_factor + 0.5) as u16,
                h: (size.1 * hidpi_factor + 0.5) as u16,
            },

            z: 0.0,
            scissor_full: true,
            prev: ObjectType::None,

            clear_texture,
            clear_sampler,
        }
    }
}

/// `gfx-rs` rendering backend.
///
/// Supports automatic batching and instancing.
///
/// Currently only OpenGL 3.2 shader set is provided. More sets, including OpenGL ES,
/// are planned.
pub struct GfxBackend<R: Resources, F: Factory<R>, C: CommandBuffer<R>> {
    /// `gfx-rs` factory.
    pub factory: F,

    /// `gfx-rs` graphics command encoder.
    pub encoder: Encoder<R, C>,

    config: Config,

    color_target: RenderTargetView<R, ColorFormat>,
    depth_target: DepthStencilView<R, DepthFormat>,

    rectangles: Rectangles<R>,
    circles: Circles<R>,
    paths: Paths<R, F, C>,
    pub(crate) texts: Texts<R>,
    effects: Effects<R>,

    projection: [[f32; 4]; 4],
    size: (f32, f32),
    hidpi_factor: f32,

    scissor: ScissorRect,
    scissor_full: bool,

    z: f32,
    prev: ObjectType,

    clear_texture: ShaderResourceView<R, [f32; 4]>,
    clear_sampler: Sampler<R>,
}

impl<R: Resources, F: Factory<R>, C: CommandBuffer<R>> GfxBackend<R, F, C> {
    /// Returns a new builder of the backend.
    pub fn build() -> GfxBackendBuilder<R, F, C> {
        GfxBackendBuilder::new()
    }

    /// Updates color target and depth stencil of the backend with provided ones.
    pub fn update_views(
        &mut self,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
    ) {
        self.color_target = color_target;
        self.depth_target = depth_target;

        let (rtv, dsv) = (self.color_target.clone(), self.depth_target.clone());
        self.pass_views(rtv, dsv);
    }

    fn pass_views(
        &mut self,
        color_target: RenderTargetView<R, ColorFormat>,
        depth_target: DepthStencilView<R, DepthFormat>,
    ) {
        self.rectangles
            .update_views(color_target.clone(), depth_target.clone());
        self.circles
            .update_views(color_target.clone(), depth_target.clone());
        self.paths
            .update_views(color_target.clone(), depth_target.clone());
        self.texts
            .update_views(color_target.clone(), depth_target.clone());
    }

    fn pass(&mut self) {
        self.rectangles.pipe.projection = self.projection;
        self.rectangles.pipe.scissor = self.scissor;

        self.circles.pipe.projection = self.projection;
        self.circles.pipe.scissor = self.scissor;

        self.paths.pipe.projection = self.projection;
        self.paths.pipe.scissor = self.scissor;

        self.texts.pipe.projection = self.projection;
        self.texts.pipe.scissor = self.scissor;
    }

    fn flush(&mut self) {
        self.pass();

        self.effects.draw(
            &mut self.encoder,
            &self.color_target,
            &self.depth_target,
            self.z - 1.0,
        );

        self.rectangles.draw(&mut self.encoder, &mut self.factory);
        self.circles.draw(&mut self.encoder, &mut self.factory);
        self.paths.draw(&mut self.encoder, &mut self.factory);
        self.texts.draw(&mut self.encoder, &mut self.factory);
    }
}

macro_rules! impl_schedule_simple {
    ($type:expr, $obj:ty, $mod:ident, $method:ident, $name: ident, $stroke_factory:path) => {
        fn $name(&mut self, state: State<Self>, $name: $obj) {
            let new_texture;

            if let Some(ref image) = state.fill.image {
                new_texture = (image.view.clone(), image.sampler.clone());
            } else {
                new_texture = (self.clear_texture.clone(), self.clear_sampler.clone());
            }

            if new_texture != self.paths.pipe.texture {
                self.$mod.draw(&mut self.encoder, &mut self.factory);
                self.$mod.pipe.texture = new_texture;
            }

            if self.prev != $type {
                self.z += 1.0;
            }

            let opaque = state.is_opaque();
            let has_texture = state.fill.image.is_some();

            if !opaque || has_texture {
                self.flush();
            }

            self.$mod.$method(state.clone(), self.z, $name);
            self.prev = $type;

            if !opaque || has_texture {
                self.flush();
            }

            if state.has_stroke() {
                let mut state = state;
                state.fill.color.a = 0.0;

                let path = $stroke_factory($name);
                self.path(state, path);
            }
        }
    }
}

impl<R: Resources, F: Factory<R>, C: CommandBuffer<R>> Backend for GfxBackend<R, F, C> {
    type Image = Image<R, F, C>;
    type Font = Font<R, F, C>;

    fn commit(&mut self) {
        self.pass();

        self.rectangles.commit(&mut self.encoder, &mut self.factory);
        self.circles.commit(&mut self.encoder, &mut self.factory);
        self.paths.commit(&mut self.encoder, &mut self.factory);
        self.texts.commit(&mut self.encoder, &mut self.factory);

        self.z = 0.0;
        self.prev = ObjectType::None;
    }

    fn resize(&mut self, size: (f32, f32)) {
        self.size = size;
        self.projection = projection(size, self.config.z_limit);

        if self.scissor_full {
            self.scissor = ScissorRect {
                x: 0,
                y: 0,
                w: (size.0 * self.hidpi_factor + 0.5) as u16,
                h: (size.1 * self.hidpi_factor + 0.5) as u16,
            };
        }

        self.effects.resize(
            &mut self.factory,
            (
                (size.0 * self.hidpi_factor + 0.5) as u16,
                (size.1 * self.hidpi_factor + 0.5) as u16,
            ),
            self.hidpi_factor,
        );
    }

    fn set_hidpi_factor(&mut self, factor: f32) {
        self.hidpi_factor = factor;

        if self.scissor_full {
            self.scissor = ScissorRect {
                x: 0,
                y: 0,
                w: (self.size.0 * self.hidpi_factor + 0.5) as u16,
                h: (self.size.1 * self.hidpi_factor + 0.5) as u16,
            };
        }
    }

    fn get_hidpi_factor(&self) -> f32 {
        self.hidpi_factor
    }

    fn set_scissor(&mut self, rect: Rect) {
        self.flush();

        self.scissor = ScissorRect {
            x: (rect.origin.x * self.hidpi_factor + 0.5) as u16,
            y: (rect.origin.y * self.hidpi_factor + 0.5) as u16,
            w: (rect.size.width * self.hidpi_factor + 0.5) as u16,
            h: (rect.size.height * self.hidpi_factor + 0.5) as u16,
        };
    }

    fn clear_scissor(&mut self) {
        self.flush();

        self.scissor_full = true;
        self.scissor = ScissorRect {
            x: 0,
            y: 0,
            w: (self.size.0 * self.hidpi_factor + 0.5) as u16,
            h: (self.size.1 * self.hidpi_factor + 0.5) as u16,
        };
    }

    fn set_effect(&mut self, effect: Effect) {
        if self.effects.effect.approx_eq(&effect) {
            return;
        }

        self.flush();
        self.effects.effect = effect;

        if let Effect::Identity = effect {
            let (rtv, dsv) = (self.color_target.clone(), self.depth_target.clone());
            self.pass_views(rtv, dsv);
        } else {
            self.effects.clear(&mut self.encoder);
            let (rtv, dsv) = (
                self.effects.entry_rtv.clone(),
                self.effects.entry_dsv.clone(),
            );
            self.pass_views(rtv, dsv);
        }
    }

    fn clear(&mut self, color: Color) {
        self.flush();

        self.encoder
            .clear(&self.color_target, [color.r, color.g, color.b, color.a]);
        self.encoder.clear_depth(&self.depth_target, 1.0);
    }

    fn path(&mut self, state: State<Self>, path: Path) {
        let opaque = state.is_opaque();
        let has_texture = state.fill.image.is_some();

        if !opaque || has_texture {
            self.flush();
        }

        if self.prev != ObjectType::Path {
            self.z += 1.0;
        }

        let new_texture;

        if let Some(ref image) = state.fill.image {
            new_texture = (image.view.clone(), image.sampler.clone());
        } else {
            new_texture = (self.clear_texture.clone(), self.clear_sampler.clone());
        }

        if new_texture != self.paths.pipe.texture {
            self.paths.draw(&mut self.encoder, &mut self.factory);
            self.paths.pipe.texture = new_texture;
        }

        self.paths.schedule(state, self.z, path);
        self.prev = ObjectType::Path;

        if !opaque || has_texture {
            self.flush();
        }
    }

    fn text(&mut self, state: State<Self>, text: Text) {
        let opaque = state.is_opaque();

        if !opaque {
            self.flush();
        }

        if self.prev != ObjectType::Text {
            self.z += 1.0;
        }

        self.texts.schedule(state, self.z, text);
        self.prev = ObjectType::Text;

        if !opaque {
            self.flush();
        }
    }

    impl_schedule_simple!(
        ObjectType::Rect,
        Rect,
        rectangles,
        schedule,
        rect,
        rect_stroke
    );

    impl_schedule_simple!(
        ObjectType::Circle,
        Circle,
        circles,
        schedule_circle,
        circle,
        circle_stroke
    );

    impl_schedule_simple!(
        ObjectType::Circle,
        Ellipse,
        circles,
        schedule_ellipse,
        ellipse,
        ellipse_stroke
    );
}

fn projection(size: (f32, f32), z: f32) -> [[f32; 4]; 4] {
    Transform3D::create_scale(2.0 / size.0, -2.0 / size.1, -1.0 / z)
        .pre_translate(Vector3D::new(-size.0 / 2.0, -size.1 / 2.0, -z))
        .to_row_arrays()
}

fn rect_stroke(rect: Rect) -> Path {
    let mut path = Path::new();
    path.move_to(rect.origin);
    path.line_to(rect.top_right());
    path.line_to(rect.bottom_right());
    path.line_to(rect.bottom_left());
    path.close();

    path
}

fn circle_stroke(circle: Circle) -> Path {
    let mut path = Path::new();
    path.move_to(circle.origin + Vector2D::new(0.0, 1.0) * circle.radius);
    path.arc(circle.origin, circle.radius, circle.radius, 1.0 * PI, 0.0);
    path.arc(circle.origin, circle.radius, circle.radius, 1.0 * PI, 0.0);
    path.close();

    path
}

fn ellipse_stroke(ellipse: Ellipse) -> Path {
    let mut path = Path::new();
    path.move_to(ellipse.origin + Vector2D::new(0.0, 1.0) * ellipse.radius_y);
    path.arc(
        ellipse.origin,
        ellipse.radius_x,
        ellipse.radius_y,
        1.0 * PI,
        0.0,
    );
    path.arc(
        ellipse.origin,
        ellipse.radius_x,
        ellipse.radius_y,
        1.0 * PI,
        0.0,
    );
    path.close();

    path
}
