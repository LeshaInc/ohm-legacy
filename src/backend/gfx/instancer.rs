use std::marker::PhantomData;

use gfx::buffer::Role;
use gfx::format::Format;
use gfx::handle::Buffer;
use gfx::memory::{Bind, Pod, Usage};
use gfx::pso::buffer::Structure;
use gfx::pso::{PipelineData, PipelineInit, PipelineState};
use gfx::traits::FactoryExt;
use gfx::{CommandBuffer, Encoder, Factory, Resources, Slice};

use euclid::approxeq::ApproxEq;

pub struct InstancerBuilder<P, V, I>
where
    P: PipelineInit,
    V: Pod + Structure<Format> + 'static,
    I: Pod + ApproxEq<f32>,
{
    capacity: usize,
    vert_shader: Option<&'static [u8]>,
    frag_shader: Option<&'static [u8]>,
    init: Option<P>,
    vertices: Option<&'static [V]>,
    indices: Option<&'static [u16]>,
    _marker: PhantomData<I>,
}

impl<P, V, I> InstancerBuilder<P, V, I>
where
    P: PipelineInit,
    V: Pod + Structure<Format> + 'static,
    I: Pod + ApproxEq<f32>,
{
    pub fn new() -> InstancerBuilder<P, V, I> {
        InstancerBuilder {
            capacity: 0,
            vert_shader: None,
            frag_shader: None,
            init: None,
            vertices: None,
            indices: None,
            _marker: Default::default(),
        }
    }

    pub fn with_pipeline(
        mut self,
        vs: &'static [u8],
        fs: &'static [u8],
        init: P,
    ) -> InstancerBuilder<P, V, I> {
        self.vert_shader = Some(vs);
        self.frag_shader = Some(fs);
        self.init = Some(init);
        self
    }

    pub fn with_geometry(
        mut self,
        vertices: &'static [V],
        indices: Option<&'static [u16]>,
    ) -> InstancerBuilder<P, V, I> {
        self.vertices = Some(vertices);
        self.indices = indices;
        self
    }

    pub fn with_capacity(mut self, capacity: usize) -> InstancerBuilder<P, V, I> {
        self.capacity = capacity;
        self
    }

    pub fn build<R: Resources, F: Factory<R>>(
        self,
        factory: &mut F,
    ) -> Instancer<R, P::Meta, V, I> {
        let cap = self.capacity;

        let (vertex_buffer, slice) = match self.indices {
            Some(v) => factory.create_vertex_buffer_with_slice(self.vertices.unwrap(), v),
            None => factory.create_vertex_buffer_with_slice(self.vertices.unwrap(), ()),
        };

        let instance_buffer = factory
            .create_buffer(cap, Role::Vertex, Usage::Dynamic, Bind::empty())
            .unwrap();

        let pso = factory
            .create_pipeline_simple(
                self.vert_shader.unwrap(),
                self.frag_shader.unwrap(),
                self.init.unwrap(),
            )
            .unwrap();

        Instancer {
            vertex_buffer,
            instance_buffer,
            slice,
            pso,
            instances: Vec::with_capacity(cap),
            upd_offset: 0,
            upd_len: 0,
            draw_offset: 0,
            draw_len: 0,
            pos: 0,
        }
    }
}

pub struct Instancer<R: Resources, M, V, I: Pod + ApproxEq<f32>> {
    pub vertex_buffer: Buffer<R, V>,
    pub instance_buffer: Buffer<R, I>,

    slice: Slice<R>,
    instances: Vec<I>,
    pso: PipelineState<R, M>,

    upd_offset: usize,
    upd_len: usize,
    draw_offset: u32,
    draw_len: u32,
    pos: usize,
}

impl<R: Resources, M, V, I: Pod + ApproxEq<f32>> Instancer<R, M, V, I> {
    pub fn schedule(&mut self, instance: I) {
        let update = self
            .instances
            .get(self.pos)
            .filter(|v| v.approx_eq(&instance))
            .is_none();

        if update {
            if self.upd_len == 0 {
                self.upd_offset = self.pos;
            }

            self.upd_len += 1;

            if self.pos >= self.instances.len() {
                self.instances.push(instance);
            } else {
                self.instances[self.pos] = instance;
            }
        }

        self.pos += 1;
        self.draw_len += 1;
    }

    pub fn update<C, F>(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F)
    where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        let instance_count = self.instances.len();
        let buf_len = self.instance_buffer.len();

        if buf_len != instance_count {
            self.instance_buffer = factory
                .create_buffer(instance_count, Role::Vertex, Usage::Dynamic, Bind::empty())
                .unwrap();

            encoder
                .update_buffer(&self.instance_buffer, &self.instances[..], 0)
                .unwrap();
        } else if self.upd_len > 0 {
            let slice = &self.instances[self.upd_offset..(self.upd_offset + self.upd_len)];

            encoder
                .update_buffer(&self.instance_buffer, slice, self.upd_offset)
                .unwrap();
        }

        self.upd_offset = 0;
        self.upd_len = 0;
    }

    pub fn draw<D, C>(&mut self, data: &D, encoder: &mut Encoder<R, C>)
    where
        D: PipelineData<R, Meta = M>,
        C: CommandBuffer<R>,
    {
        self.slice.instances = Some((self.draw_len, self.draw_offset));

        encoder.draw(&self.slice, &self.pso, data);

        self.draw_offset += self.draw_len;
        self.draw_len = 0;
    }

    pub fn commit(&mut self) {
        self.draw_offset = 0;
        self.draw_len = 0;
        self.upd_offset = 0;
        self.upd_len = 0;
        self.pos = 0;
    }
}
