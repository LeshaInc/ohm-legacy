use std::collections::HashMap;

/// Shader language with version specified
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum ShaderApi {
    /// OpenGL Shading Language.
    ///
    /// The version field is the version of the GLSL itself, for example GLSL 150
    /// corresponds to OpenGL 3.2, while GLSL 330 to OpenGL 3.3.
    Glsl(u16),

    /// OpenGL ES Shading Language.
    GlslEs(u16),

    /// Direct3D Shading Language (High-Level Shading Language).
    Hlsl(u16),

    /// Metal Shading Language.
    Mtl(u16),

    /// Vulkan Shading Language (GLSL 460).
    Vulkan,
}

/// A structure containing shader sources for different backends.
#[derive(Debug, Clone)]
pub struct ShaderSource {
    map: HashMap<ShaderApi, &'static [u8]>,
}

impl ShaderSource {
    /// Creates a new, empty set of shaders.
    pub fn new() -> ShaderSource {
        ShaderSource {
            map: HashMap::new(),
        }
    }

    /// Adds a new shader source to the set.
    ///
    /// If the specified api did present in the set, the source is overwritten.
    pub fn with(mut self, api: ShaderApi, source: &'static [u8]) -> ShaderSource {
        self.map.insert(api, source);
        self
    }

    /// Returns shader source with the version less or equal to the specified.
    ///
    /// # Panics
    ///
    /// Panics if the set does not have the specified shader api.
    pub fn select(&self, api: ShaderApi) -> &'static [u8] {
        let selected = self
            .map
            .keys()
            .filter(|&&k| k <= api)
            .max()
            .expect("unsupported shader api");

        self.map[selected]
    }
}
