use std::marker::PhantomData;

use gfx::{CommandBuffer, Factory, Resources};

use rusttype::FontCollection;

use super::GfxBackend;
use backend;

/// `gfx-rs` backend font handle.
///
/// Encapsulates an id.
#[derive(Debug)]
pub struct Font<R, F, C>
where
    R: Resources,
    C: CommandBuffer<R>,
    F: Factory<R>,
{
    pub(crate) id: usize,
    _marker: PhantomData<(R, F, C)>,
}

impl<R, F, C> Clone for Font<R, F, C>
where
    R: Resources,
    C: CommandBuffer<R>,
    F: Factory<R>,
{
    fn clone(&self) -> Font<R, F, C> {
        Font {
            id: self.id,
            _marker: Default::default(),
        }
    }
}

impl<R, F, C> backend::Font for Font<R, F, C>
where
    R: Resources,
    C: CommandBuffer<R>,
    F: Factory<R>,
{
    type Backend = GfxBackend<R, F, C>;

    fn from_bytes(
        backend: &mut Self::Backend,
        bytes: Vec<u8>,
        index: Option<usize>,
    ) -> Font<R, F, C> {
        let collection = FontCollection::from_bytes(bytes.to_owned()).unwrap();

        let font = collection
            .into_fonts()
            .nth(index.unwrap_or(0))
            .unwrap()
            .unwrap();
        backend.texts.fonts.push(font);

        let id = backend.texts.fonts.len() - 1;

        Font {
            id,
            _marker: Default::default(),
        }
    }
}
