use std::marker::PhantomData;

use gfx::buffer::Role;
use gfx::handle::Buffer;
use gfx::memory::{Bind, Pod, Usage};
use gfx::pso::{PipelineData, PipelineInit, PipelineState};
use gfx::traits::FactoryExt;
use gfx::{CommandBuffer, Encoder, Factory, IndexBuffer, Resources, Slice};

pub struct BatcherBuilder<P: PipelineInit, V: Pod> {
    vbuf_capacity: usize,
    ibuf_capacity: usize,
    vert_shader: Option<&'static [u8]>,
    frag_shader: Option<&'static [u8]>,
    init: Option<P>,
    _marker: PhantomData<V>,
}

impl<P: PipelineInit, V: Pod> BatcherBuilder<P, V> {
    pub fn new() -> BatcherBuilder<P, V> {
        BatcherBuilder {
            vbuf_capacity: 0,
            ibuf_capacity: 0,
            vert_shader: None,
            frag_shader: None,
            init: None,
            _marker: Default::default(),
        }
    }

    pub fn with_pipeline(
        mut self,
        vs: &'static [u8],
        fs: &'static [u8],
        init: P,
    ) -> BatcherBuilder<P, V> {
        self.vert_shader = Some(vs);
        self.frag_shader = Some(fs);
        self.init = Some(init);
        self
    }

    pub fn with_vbuf_capacity(mut self, capacity: usize) -> BatcherBuilder<P, V> {
        self.vbuf_capacity = capacity;
        self
    }

    pub fn with_ibuf_capacity(mut self, capacity: usize) -> BatcherBuilder<P, V> {
        self.ibuf_capacity = capacity;
        self
    }

    pub fn build<R: Resources, F: Factory<R>>(self, factory: &mut F) -> Batcher<R, P::Meta, V> {
        let vertex_buffer = factory
            .create_buffer(
                self.vbuf_capacity,
                Role::Vertex,
                Usage::Dynamic,
                Bind::empty(),
            )
            .unwrap();

        let index_buffer = factory
            .create_buffer(
                self.ibuf_capacity,
                Role::Index,
                Usage::Dynamic,
                Bind::empty(),
            )
            .unwrap();

        let pso = factory
            .create_pipeline_simple(
                self.vert_shader.unwrap(),
                self.frag_shader.unwrap(),
                self.init.unwrap(),
            )
            .unwrap();

        Batcher {
            vertex_buffer,
            index_buffer,
            pso,
            vertices: Vec::with_capacity(self.vbuf_capacity),
            indices: Vec::with_capacity(self.ibuf_capacity),
            index_offset: 0,
            draw_offset: 0,
            draw_len: 0,
        }
    }
}

pub struct Batcher<R: Resources, M, V: Pod> {
    pub vertex_buffer: Buffer<R, V>,
    pub index_buffer: Buffer<R, u32>,

    pso: PipelineState<R, M>,

    vertices: Vec<V>,
    indices: Vec<u32>,

    index_offset: u32,
    draw_offset: u32,
    draw_len: u32,
}

impl<R: Resources, M, V: Pod> Batcher<R, M, V> {
    pub fn add_vertex(&mut self, vertex: V) {
        self.vertices.push(vertex);
    }

    pub fn add_index(&mut self, index: u32) {
        self.indices.push(self.index_offset + index);
        self.draw_len += 1;
    }

    pub fn next_mesh(&mut self) {
        self.index_offset = self.vertices.len() as u32;
    }

    pub fn update<C, F>(&mut self, encoder: &mut Encoder<R, C>, factory: &mut F)
    where
        C: CommandBuffer<R>,
        F: Factory<R>,
    {
        if self.vertex_buffer.len() < self.vertices.len() {
            self.vertex_buffer = factory
                .create_buffer(
                    self.vertices.len(),
                    Role::Vertex,
                    Usage::Dynamic,
                    Bind::empty(),
                )
                .unwrap();
        }

        if self.index_buffer.len() < self.indices.len() {
            self.index_buffer = factory
                .create_buffer(
                    self.indices.len(),
                    Role::Index,
                    Usage::Dynamic,
                    Bind::empty(),
                )
                .unwrap();
        }

        encoder
            .update_buffer(&self.vertex_buffer, &self.vertices[..], 0)
            .unwrap();
        encoder
            .update_buffer(&self.index_buffer, &self.indices[..], 0)
            .unwrap();
    }

    pub fn draw<D, C>(&mut self, data: &D, encoder: &mut Encoder<R, C>)
    where
        D: PipelineData<R, Meta = M>,
        C: CommandBuffer<R>,
    {
        if self.draw_len == 0 {
            return;
        }

        let slice = Slice {
            start: self.draw_offset,
            end: self.draw_offset + self.draw_len,
            base_vertex: 0,
            instances: None,
            buffer: IndexBuffer::Index32(self.index_buffer.clone()),
        };

        encoder.draw(&slice, &self.pso, data);

        self.draw_offset += self.draw_len;
        self.draw_len = 0;
    }

    pub fn commit(&mut self) {
        self.vertices.clear();
        self.indices.clear();
        self.index_offset = 0;
        self.draw_offset = 0;
        self.draw_len = 0;
    }
}
