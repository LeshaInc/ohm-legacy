//! Object styles.

use euclid::approxeq::ApproxEq;

use backend::Backend;

/// Normalized object color in the RGBA format.
#[derive(Debug, Copy, Clone)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl Into<[f32; 4]> for Color {
    fn into(self) -> [f32; 4] {
        [self.r, self.g, self.b, self.a]
    }
}

impl From<[f32; 4]> for Color {
    fn from(c: [f32; 4]) -> Color {
        Color {
            r: c[0],
            g: c[1],
            b: c[2],
            a: c[3],
        }
    }
}

impl Into<[f32; 3]> for Color {
    fn into(self) -> [f32; 3] {
        [self.r, self.g, self.b]
    }
}

impl From<[f32; 3]> for Color {
    fn from(c: [f32; 3]) -> Color {
        Color {
            r: c[0],
            g: c[1],
            b: c[2],
            a: 1.0,
        }
    }
}

/// Object fill.
pub struct Fill<B: Backend> {
    pub color: Color,
    pub image: Option<B::Image>,
}

impl<B: Backend> Clone for Fill<B> {
    fn clone(&self) -> Fill<B> {
        Fill {
            color: self.color,
            image: self.image.clone(),
        }
    }
}

impl<B: Backend> Fill<B> {
    /// Returns true if the fill would not actually render, e.g. the transparent fill.
    pub fn is_empty(&self) -> bool {
        self.color.a.approx_eq(&0.0)
    }
}

impl<B: Backend> ApproxEq<f32> for Fill<B> {
    fn approx_epsilon() -> f32 {
        f32::approx_epsilon()
    }

    fn approx_eq(&self, other: &Fill<B>) -> bool {
        self.approx_eq_eps(other, &Fill::<B>::approx_epsilon())
    }

    fn approx_eq_eps(&self, other: &Fill<B>, eps: &f32) -> bool {
        self.image == other.image
            && self.color.r.approx_eq_eps(&other.color.r, eps)
            && self.color.g.approx_eq_eps(&other.color.g, eps)
            && self.color.b.approx_eq_eps(&other.color.b, eps)
            && self.color.a.approx_eq_eps(&other.color.a, eps)
    }
}

impl<B: Backend> Default for Fill<B> {
    fn default() -> Fill<B> {
        Fill {
            color: [0.0, 0.0, 0.0, 1.0].into(),
            image: None,
        }
    }
}

/// Object stroke.
#[derive(Debug, Copy, Clone)]
pub struct Stroke {
    pub color: Color,
    pub thickness: f32,
}

impl Stroke {
    /// Returns true if the stroke would not actually render, e.g. the thickness is set to 0.
    pub fn is_empty(&self) -> bool {
        self.thickness.approx_eq(&0.0)
    }
}

// why did I implement this?
impl ApproxEq<f32> for Stroke {
    fn approx_epsilon() -> f32 {
        f32::approx_epsilon()
    }

    fn approx_eq(&self, other: &Stroke) -> bool {
        self.approx_eq_eps(other, &Stroke::approx_epsilon())
    }

    fn approx_eq_eps(&self, other: &Stroke, eps: &f32) -> bool {
        self.thickness.approx_eq_eps(&other.thickness, eps)
            && self.color.r.approx_eq_eps(&other.color.r, eps)
            && self.color.g.approx_eq_eps(&other.color.g, eps)
            && self.color.b.approx_eq_eps(&other.color.b, eps)
            && self.color.a.approx_eq_eps(&other.color.a, eps)
    }
}

impl Default for Stroke {
    fn default() -> Stroke {
        Stroke {
            color: [0.0, 0.0, 0.0, 1.0].into(),
            thickness: 0.0,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum Effect {
    GaussianBlur(f32),
    Identity,
}

impl ApproxEq<f32> for Effect {
    fn approx_epsilon() -> f32 {
        f32::approx_epsilon()
    }

    fn approx_eq(&self, other: &Effect) -> bool {
        self.approx_eq_eps(other, &Effect::approx_epsilon())
    }

    fn approx_eq_eps(&self, other: &Effect, eps: &f32) -> bool {
        use Effect::*;

        match (*self, *other) {
            (GaussianBlur(a), GaussianBlur(b)) => a.approx_eq_eps(&b, eps),
            (Identity, Identity) => true,
            _ => false,
        }
    }
}
