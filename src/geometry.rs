//! Type defenitions of geometry types using the `euclid` library.

use euclid;

/// Scalar type
pub type Scalar = f32;

/// A point in 2D space.
pub type Point2D = euclid::Point2D<Scalar>;

/// A point in 3D space.
pub type Point3D = euclid::Point3D<Scalar>;

/// A flat rectangle.
pub type Rect = euclid::Rect<Scalar>;

/// A structure representing a 2D size.
pub type Size2D = euclid::Size2D<Scalar>;

/// 2D transformation matrix.
pub type Transform2D = euclid::Transform2D<Scalar>;

/// 3D transformation matrix.
pub type Transform3D = euclid::Transform3D<Scalar>;

/// 2D vector.
pub type Vector2D = euclid::Vector2D<Scalar>;

/// 3D vector.
pub type Vector3D = euclid::Vector3D<Scalar>;

/// 3D homogeneous vector.
pub type HomogeneousVector = euclid::HomogeneousVector<Scalar, euclid::UnknownUnit>;
