//! Objects that can be drawn onto the screen.

use lyon::path::iterator::PathIter;
use lyon::path::{FlattenedEvent, PathEvent, QuadraticEvent, SvgEvent};
use lyon::tessellation::geometry_builder::{BuffersBuilder, VertexBuffers};
use lyon::tessellation::{
    FillOptions, FillTessellator, FillVertex, StrokeOptions, StrokeTessellator, StrokeVertex,
};

use euclid::approxeq::ApproxEq;
use euclid::Angle;

use backend::Backend;
use geometry::{Point2D, Scalar, Size2D, Transform2D, Vector2D};
use style::{Fill, Stroke};

pub use geometry::Rect;

/// Rectangle constructor.
pub fn rect(x: f32, y: f32, w: f32, h: f32) -> Rect {
    Rect::new(Point2D::new(x, y), Size2D::new(w, h))
}

/// A circle defined by the origin and the radius.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Circle {
    pub origin: Point2D,
    pub radius: Scalar,
}

/// Circle constructor.
pub fn circle(x: Scalar, y: Scalar, r: Scalar) -> Circle {
    Circle {
        origin: Point2D::new(x, y),
        radius: r,
    }
}

/// A ellipse defined by the origin, x axis radius, and y axis radius.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Ellipse {
    pub origin: Point2D,
    pub radius_x: Scalar,
    pub radius_y: Scalar,
}

/// Ellipse constructor.
pub fn ellipse(x: Scalar, y: Scalar, rx: Scalar, ry: Scalar) -> Ellipse {
    Ellipse {
        origin: Point2D::new(x, y),
        radius_x: rx,
        radius_y: ry,
    }
}

/// A simple command executed on paths.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PathCommand {
    /// Change current position to the specified point.
    MoveTo(Point2D),

    /// Draw a line from the current position to the specified point.
    ///
    /// The current position becomes the endpoint of the line after the operation.
    LineTo(Point2D),

    /// Draw a cubic bezier curve from the current position, to the last point in the
    /// tuple. The two other ones are control points.
    ///
    /// The current position becomes the endpoint of the curve after the operation.
    CubicCurveTo(Point2D, Point2D, Point2D),

    /// Draw a quadratic bezier curve from the current position, to the second point in the
    /// tuple. The other one is control point.
    ///
    /// The current position becomes the endpoint of the curve after the operation.
    QuadraticCurveTo(Point2D, Point2D),

    /// Draw a line from the current position to the starting point of the arc
    /// and the arc itself.
    ///
    /// The current position becomes the endpoint of the arc after the operation.
    Arc {
        /// The middle point of the arc.
        origin: Point2D,

        /// The x radius of the arc.
        radius_x: Scalar,

        /// The y radius of the arc.
        radius_y: Scalar,

        /// The degree measure of the arc.
        sweep_angle: Scalar,

        /// The rotation of the arc.
        rotation: Scalar,
    },

    /// Draw a line from the current position to the first position.
    Close,
}

impl ApproxEq<f32> for PathCommand {
    fn approx_epsilon() -> f32 {
        f32::approx_epsilon()
    }

    fn approx_eq(&self, other: &PathCommand) -> bool {
        self.approx_eq_eps(other, &PathCommand::approx_epsilon())
    }

    fn approx_eq_eps(&self, other: &PathCommand, eps: &f32) -> bool {
        use self::PathCommand::*;

        match (*self, *other) {
            (MoveTo(a), MoveTo(b)) => a.approx_eq_eps(&b, &Point2D::new(*eps, *eps)),
            (LineTo(a), LineTo(b)) => a.approx_eq_eps(&b, &Point2D::new(*eps, *eps)),

            (CubicCurveTo(a, b, c), CubicCurveTo(d, e, f)) => {
                a.approx_eq_eps(&d, &Point2D::new(*eps, *eps))
                    && b.approx_eq_eps(&e, &Point2D::new(*eps, *eps))
                    && c.approx_eq_eps(&f, &Point2D::new(*eps, *eps))
            }

            (QuadraticCurveTo(a, b), QuadraticCurveTo(c, d)) => {
                a.approx_eq_eps(&c, &Point2D::new(*eps, *eps))
                    && b.approx_eq_eps(&d, &Point2D::new(*eps, *eps))
            }

            (
                Arc {
                    origin: a_origin,
                    radius_x: a_radius_x,
                    radius_y: a_radius_y,
                    sweep_angle: a_sweep_angle,
                    rotation: a_rotation,
                },
                Arc {
                    origin: b_origin,
                    radius_x: b_radius_x,
                    radius_y: b_radius_y,
                    sweep_angle: b_sweep_angle,
                    rotation: b_rotation,
                },
            ) => {
                a_origin.approx_eq_eps(&b_origin, &Point2D::new(*eps, *eps))
                    && a_radius_x.approx_eq_eps(&b_radius_x, eps)
                    && a_radius_y.approx_eq_eps(&b_radius_y, eps)
                    && a_sweep_angle.approx_eq_eps(&b_sweep_angle, eps)
                    && a_rotation.approx_eq_eps(&b_rotation, eps)
            }

            (Close, Close) => true,

            _ => false,
        }
    }
}

/// An extended path command with the interface like the SVG one.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PathCommandExt {
    /// Change current position to the specified point.
    MoveTo(Point2D),

    /// The same as [`MoveTo`](PathCommandExt::MoveTo) but with relative coordinates.
    RelativeMoveTo(Vector2D),

    /// Draw a line from the current position to the specified point.
    ///
    /// The current position becomes the endpoint of the line after the operation.
    LineTo(Point2D),

    /// The same as [`LineTo`](PathCommandExt::LineTo) but with relative coordinates.
    RelativeLineTo(Vector2D),

    /// Draw a cubic bezier curve from the current position, to the last point in the
    /// tuple. The two other ones are control points.
    ///
    /// The current position becomes the endpoint of the curve after the operation.
    CubicCurveTo(Point2D, Point2D, Point2D),

    /// The same as [`CubicCurveTo`](PathCommandExt::CubicCurveTo) but with relative coordinates.
    RelativeCubicCurveTo(Vector2D, Vector2D, Vector2D),

    /// Draw a quadratic bezier curve from the current position, to the second point in the
    /// tuple. The other one is control point.
    ///
    /// The current position becomes the endpoint of the curve after the operation.
    QuadraticCurveTo(Point2D, Point2D),

    /// The same as [`QuadraticCurveTo`](PathCommandExt::QuadraticCurveTo)
    /// but with relative coordinates.
    RelativeQuadraticCurveTo(Vector2D, Vector2D),

    /// The same as [`CubicCurveTo`](PathCommandExt::CubicCurveTo)
    /// but the first control point is the reflection of the second control point of previous
    /// cubic curve.
    SmoothCubicCurveTo(Point2D, Point2D),

    /// The same as [`SmoothCubicCurveTo`](PathCommandExt::SmoothCubicCurveTo)
    /// but with relative coordinates.
    RelativeSmoothCubicCurveTo(Vector2D, Vector2D),

    /// The same as [`QuadraticCurveTo`](PathCommandExt::QuadraticCurveTo)
    /// but the control point is the reflection of control point of previous
    /// quadratic curve.
    SmoothQuadraticCurveTo(Point2D),

    /// The same as [`SmoothQuadraticCurveTo`](PathCommandExt::SmoothQuadraticCurveTo)
    /// but with relative coordinates.
    RelativeSmoothQuadraticCurveTo(Vector2D),

    /// Draw a horizontal line from the current position to the specified point on the x axis.
    ///
    /// The current position becomes the endpoint of the line after the operation.
    HorizontalLineTo(Scalar),

    /// The same as [`HorizontalLineTo`](PathCommandExt::HorizontalLineTo)
    /// but with relative coordinates.
    RelativeHorizontalLineTo(Scalar),

    /// Draw a vertical line from the current position to the specified point on the x axis.
    ///
    /// The current position becomes the endpoint of the line after the operation.
    VerticalLineTo(Scalar),

    /// The same as [`VerticalLineTo`](PathCommandExt::VerticalLineTo)
    /// but with relative coordinates.
    RelativeVerticalLineTo(Scalar),

    /// Draw a line from the current position to the starting point of the arc
    /// and the arc itself.
    ///
    /// The current position becomes the endpoint of the arc after the operation.
    Arc {
        /// The middle point of the arc.
        origin: Point2D,

        /// The x radius of the arc.
        radius_x: Scalar,

        /// The y radius of the arc.
        radius_y: Scalar,

        /// The degree measure of the arc.
        sweep_angle: Scalar,

        /// The rotation of the arc.
        rotation: Scalar,
    },

    /// Draw a line from the current position to the first position.
    Close,
}

/// A path.
#[derive(Debug, Clone)]
pub struct Path {
    commands: Vec<PathCommand>,
    start: Option<Point2D>,
    position: Point2D,
}

impl Path {
    /// Creates a new path.
    pub fn new() -> Path {
        Path::with_capacity(0)
    }

    /// Creates a new path with specified vector capacity.
    pub fn with_capacity(cap: usize) -> Path {
        Path {
            commands: Vec::with_capacity(cap),
            start: None,
            position: Point2D::new(0.0, 0.0),
        }
    }

    /// Executes a simple command on the path.
    pub fn command<C: Into<PathCommand>>(&mut self, command: C) {
        self.commands.push(command.into());
    }

    /// Executes an extended command on the path.
    pub fn command_ext<C: Into<PathCommandExt>>(&mut self, command: C) {
        let command: PathCommandExt = command.into();

        use self::PathCommandExt::*;

        match command {
            MoveTo(p) => self.move_to(p),
            RelativeMoveTo(v) => self.relative_move_to(v),
            LineTo(p) => self.line_to(p),
            RelativeLineTo(v) => self.relative_line_to(v),
            CubicCurveTo(c1, c2, p) => self.cubic_curve_to(c1, c2, p),
            RelativeCubicCurveTo(c1, c2, v) => self.relative_cubic_curve_to(c1, c2, v),
            QuadraticCurveTo(c, p) => self.quadratic_curve_to(c, p),
            RelativeQuadraticCurveTo(c, v) => self.relative_quadratic_curve_to(c, v),
            SmoothCubicCurveTo(c, p) => self.smooth_cubic_curve_to(c, p),
            RelativeSmoothCubicCurveTo(c, v) => self.relative_smooth_cubic_curve_to(c, v),
            SmoothQuadraticCurveTo(p) => self.smooth_quadratic_curve_to(p),
            RelativeSmoothQuadraticCurveTo(v) => self.relative_smooth_quadratic_curve_to(v),
            HorizontalLineTo(d) => self.horizontal_line_to(d),
            RelativeHorizontalLineTo(d) => self.relative_horizontal_line_to(d),
            VerticalLineTo(d) => self.vertical_line_to(d),
            RelativeVerticalLineTo(d) => self.relative_vertical_line_to(d),
            Arc {
                origin,
                radius_x,
                radius_y,
                sweep_angle,
                rotation,
            } => self.arc(origin, radius_x, radius_y, sweep_angle, rotation),
            Close => self.close(),
        }
    }

    /// Changes current position to the specified point.
    pub fn move_to(&mut self, point: Point2D) {
        self.command(PathCommand::MoveTo(point));
        self.position = point;

        if let None = self.start {
            self.start = Some(point);
        }
    }

    /// The same as [`move_to`](Path::move_to) but with relative coordinates.
    pub fn relative_move_to(&mut self, vector: Vector2D) {
        let pos = self.position;
        self.move_to(pos + vector);
    }

    /// Draws a line from the current position to the specified point.
    ///
    /// The current position becomes the endpoint of the line after the operation.
    pub fn line_to(&mut self, endpoint: Point2D) {
        self.command(PathCommand::LineTo(endpoint));
        self.position = endpoint;
    }

    /// The same as [`line_to`](Path::line_to) but with relative coordinates.
    pub fn relative_line_to(&mut self, vector: Vector2D) {
        let pos = self.position;
        self.line_to(pos + vector);
    }

    /// Draws a cubic bezier curve from the current position, to the last point in the
    /// tuple. The two other ones are control points.
    ///
    /// The current position becomes the endpoint of the curve after the operation.
    pub fn cubic_curve_to(&mut self, c1: Point2D, c2: Point2D, endpoint: Point2D) {
        self.command(PathCommand::CubicCurveTo(c1, c2, endpoint));
        self.position = endpoint;
    }

    /// The same as [`cubic_curve_to`](Path::cubic_curve_to) but with relative coordinates.
    pub fn relative_cubic_curve_to(&mut self, c1: Vector2D, c2: Vector2D, vector: Vector2D) {
        let pos = self.position;
        self.cubic_curve_to(pos + c1, pos + c2, pos + vector);
    }

    /// Draws a quadratic bezier curve from the current position, to the second point in the
    /// tuple. The other one is control point.
    ///
    /// The current position becomes the endpoint of the curve after the operation.
    pub fn quadratic_curve_to(&mut self, control: Point2D, endpoint: Point2D) {
        self.command(PathCommand::QuadraticCurveTo(control, endpoint));
        self.position = endpoint;
    }

    /// The same as [`quadratic_curve_to`](Path::quadratic_curve_to) but with relative coordinates.
    pub fn relative_quadratic_curve_to(&mut self, control: Vector2D, vector: Vector2D) {
        let pos = self.position;
        self.quadratic_curve_to(pos + control, pos + vector);
    }

    /// Draws a line from the current position to the starting point of the arc
    /// and the arc itself.
    ///
    /// The current position becomes the endpoint of the arc after the operation.
    pub fn arc(
        &mut self,
        origin: Point2D,
        rx: Scalar,
        ry: Scalar,
        sweep_angle: Scalar,
        rotation: Scalar,
    ) {
        self.command(PathCommand::Arc {
            radius_x: rx,
            radius_y: ry,
            origin,
            sweep_angle,
            rotation,
        });
        let vec = (origin - self.position).normalize();
        let rotated = Transform2D::create_rotation(Angle::radians(sweep_angle + rotation))
            .transform_vector(&vec);
        let translation = Vector2D::new(rotated.x * rx, rotated.y * ry);
        self.position = origin + translation;
    }

    // TODO: how do we implement this?
    // pub fn arc_to(&mut self) {

    // }

    // TODO: how do we implement this? ;)
    // pub fn relative_arc_to(&mut self) {

    // }

    /// The same as [`cubic_curve_to`](Path::cubic_curve_to)
    /// but the first control point is the reflection of the second control point of previous
    /// cubic curve.
    pub fn smooth_cubic_curve_to(&mut self, control: Point2D, endpoint: Point2D) {
        let prev = match self.commands.last() {
            Some(&PathCommand::CubicCurveTo(_, control, _)) => control,
            _ => self.position,
        };

        let vec = self.position - prev;
        let refl = prev + vec.normalize() * (2.0 * vec.length());

        self.cubic_curve_to(refl, control, endpoint);
    }

    /// The same as [`smooth_cubic_curve_to`](Path::smooth_cubic_curve_to)
    /// but with relative coordinates.
    pub fn relative_smooth_cubic_curve_to(&mut self, control: Vector2D, vector: Vector2D) {
        let pos = self.position;
        self.smooth_cubic_curve_to(pos + control, pos + vector);
    }

    /// The same as [`quadratic_curve_to`](Path::quadratic_curve_to)
    /// but the control point is the reflection of control point of previous
    /// quadratic curve.
    pub fn smooth_quadratic_curve_to(&mut self, endpoint: Point2D) {
        let prev = match self.commands.last() {
            Some(&PathCommand::QuadraticCurveTo(control, _)) => control,
            _ => self.position,
        };

        let vec = prev - self.position;
        let refl = prev + vec.normalize() * (2.0 * vec.length());

        self.quadratic_curve_to(refl, endpoint);
    }

    /// The same as [`smooth_quadratic_curve_to`](Path::smooth_quadratic_curve_to)
    /// but with relative coordinates.
    pub fn relative_smooth_quadratic_curve_to(&mut self, vector: Vector2D) {
        let pos = self.position;
        self.smooth_quadratic_curve_to(pos + vector);
    }

    /// Draws a horizontal line from the current position to the specified point on the x axis.
    ///
    /// The current position becomes the endpoint of the line after the operation.
    pub fn horizontal_line_to(&mut self, x: Scalar) {
        let pos = self.position;
        self.command(PathCommand::LineTo(Point2D::new(x, pos.y)));
        self.position = Point2D::new(x, pos.y);
    }

    /// The same as [`horizontal_line_to`](Path::horizontal_line_to)
    /// but with relative coordinates.
    pub fn relative_horizontal_line_to(&mut self, x: Scalar) {
        let pos = self.position;
        self.horizontal_line_to(pos.x + x);
    }

    /// Draws a vertical line from the current position to the specified point on the x axis.
    ///
    /// The current position becomes the endpoint of the line after the operation.
    pub fn vertical_line_to(&mut self, y: Scalar) {
        let pos = self.position;
        self.command(PathCommand::LineTo(Point2D::new(pos.x, y)));
        self.position = Point2D::new(pos.x, y);
    }

    /// The same as [`vertical_line_to`](Path::vertical_line_to)
    /// but with relative coordinates.
    pub fn relative_vertical_line_to(&mut self, y: Scalar) {
        let pos = self.position;
        self.vertical_line_to(pos.y + y);
    }

    /// Draws a line from the current position to the first position.
    pub fn close(&mut self) {
        self.command(PathCommand::Close);
        self.position = self.start.unwrap_or(Point2D::new(0.0, 0.0));
        self.start = None;
    }

    /// The current position
    pub fn position(&self) -> Point2D {
        self.position
    }

    fn gen_uv(vertices: &mut Vec<Vertex>) -> Rect {
        let (mut min, mut max) = (Point2D::new(0.0, 0.0), Point2D::new(0.0, 0.0));

        for v in vertices.iter() {
            min = min.min(v.position.into());
            max = max.max(v.position.into());
        }

        let bbox = Rect::new(min, Size2D::new(max.x - min.x, max.y - min.y));

        let tx = -bbox.origin.x;
        let ty = -bbox.origin.y;
        let sx = bbox.size.width;
        let sy = bbox.size.height;

        for v in vertices.iter_mut() {
            v.uv = [(v.position[0] + tx) / sx, (v.position[1] + ty) / sy];
        }

        bbox
    }

    fn tessellate_fill(&self) -> TessellatedPathPart {
        let iter = PathIter::new(self.commands.iter().map(|&v| v));
        let options = FillOptions::default().with_normals(false);

        let mut geometry = VertexBuffers::new();

        {
            let mut builder = BuffersBuilder::new(&mut geometry, |v: FillVertex| Vertex {
                position: v.position.to_array(),
                uv: [0.0; 2],
            });

            let mut tessellator = FillTessellator::new();
            tessellator
                .tessellate_path(iter, &options, &mut builder)
                .unwrap();
        }

        let bounding_box = Path::gen_uv(&mut geometry.vertices);

        TessellatedPathPart {
            vertices: geometry.vertices,
            indices: geometry.indices,
            bounding_box,
        }
    }

    fn tessellate_stroke(&self, stroke: Stroke) -> TessellatedPathPart {
        let iter = PathIter::new(self.commands.iter().map(|&v| v));
        let options = StrokeOptions::default().with_line_width(stroke.thickness);

        let mut geometry = VertexBuffers::new();

        {
            let mut builder = BuffersBuilder::new(&mut geometry, |v: StrokeVertex| Vertex {
                position: v.position.to_array(),
                uv: [0.0; 2],
            });

            let mut tessellator = StrokeTessellator::new();
            tessellator.tessellate_path(iter, &options, &mut builder);
        }

        let bounding_box = Path::gen_uv(&mut geometry.vertices);

        TessellatedPathPart {
            vertices: geometry.vertices,
            indices: geometry.indices,
            bounding_box,
        }
    }

    /// Tessellates the path.
    pub fn tessellate<B: Backend>(&self, fill: Fill<B>, stroke: Stroke) -> TessellatedPath {
        let fill = if fill.is_empty() {
            None
        } else {
            Some(self.tessellate_fill())
        };

        let stroke = if stroke.is_empty() {
            None
        } else {
            Some(self.tessellate_stroke(stroke))
        };

        TessellatedPath { fill, stroke }
    }
}

impl ApproxEq<f32> for Path {
    fn approx_epsilon() -> f32 {
        f32::approx_epsilon()
    }

    fn approx_eq(&self, other: &Path) -> bool {
        self.approx_eq_eps(other, &Path::approx_epsilon())
    }

    fn approx_eq_eps(&self, other: &Path, eps: &f32) -> bool {
        if self.commands.len() != other.commands.len() {
            return false;
        }

        self.commands
            .iter()
            .zip(other.commands.iter())
            .all(|(a, b)| a.approx_eq_eps(b, eps))
    }
}

impl Into<PathEvent> for PathCommand {
    fn into(self) -> PathEvent {
        match self {
            PathCommand::MoveTo(p) => PathEvent::MoveTo(p),
            PathCommand::LineTo(p) => PathEvent::LineTo(p),
            PathCommand::CubicCurveTo(c1, c2, p) => PathEvent::CubicTo(c1, c2, p),
            PathCommand::QuadraticCurveTo(c, p) => PathEvent::QuadraticTo(c, p),
            PathCommand::Arc {
                origin,
                radius_x,
                radius_y,
                sweep_angle,
                rotation,
            } => PathEvent::Arc(
                origin,
                Vector2D::new(radius_x, radius_y),
                Angle::radians(sweep_angle),
                Angle::radians(rotation),
            ),
            PathCommand::Close => PathEvent::Close,
        }
    }
}

impl From<PathCommand> for PathCommandExt {
    fn from(c: PathCommand) -> PathCommandExt {
        match c {
            PathCommand::MoveTo(p) => PathCommandExt::MoveTo(p),
            PathCommand::LineTo(p) => PathCommandExt::LineTo(p),
            PathCommand::CubicCurveTo(c1, c2, p) => PathCommandExt::CubicCurveTo(c1, c2, p),
            PathCommand::QuadraticCurveTo(c, p) => PathCommandExt::QuadraticCurveTo(c, p),
            PathCommand::Arc {
                origin,
                radius_x,
                radius_y,
                sweep_angle,
                rotation,
            } => PathCommandExt::Arc {
                origin,
                radius_x,
                radius_y,
                sweep_angle,
                rotation,
            },
            PathCommand::Close => PathCommandExt::Close,
        }
    }
}

impl From<PathEvent> for PathCommandExt {
    fn from(c: PathEvent) -> PathCommandExt {
        match c {
            PathEvent::MoveTo(p) => PathCommandExt::MoveTo(p),
            PathEvent::LineTo(p) => PathCommandExt::LineTo(p),
            PathEvent::CubicTo(c1, c2, p) => PathCommandExt::CubicCurveTo(c1, c2, p),
            PathEvent::QuadraticTo(c, p) => PathCommandExt::QuadraticCurveTo(c, p),
            PathEvent::Arc(origin, v, sw, rot) => PathCommandExt::Arc {
                origin,
                radius_x: v.x,
                radius_y: v.y,
                sweep_angle: sw.get(),
                rotation: rot.get(),
            },
            PathEvent::Close => PathCommandExt::Close,
        }
    }
}

impl From<FlattenedEvent> for PathCommandExt {
    fn from(c: FlattenedEvent) -> PathCommandExt {
        match c {
            FlattenedEvent::MoveTo(p) => PathCommandExt::MoveTo(p),
            FlattenedEvent::LineTo(p) => PathCommandExt::LineTo(p),
            FlattenedEvent::Close => PathCommandExt::Close,
        }
    }
}

impl From<QuadraticEvent> for PathCommandExt {
    fn from(c: QuadraticEvent) -> PathCommandExt {
        match c {
            QuadraticEvent::MoveTo(p) => PathCommandExt::MoveTo(p),
            QuadraticEvent::LineTo(p) => PathCommandExt::LineTo(p),
            QuadraticEvent::QuadraticTo(c, p) => PathCommandExt::QuadraticCurveTo(c, p),
            QuadraticEvent::Close => PathCommandExt::Close,
        }
    }
}

impl From<SvgEvent> for PathCommandExt {
    fn from(c: SvgEvent) -> PathCommandExt {
        match c {
            SvgEvent::MoveTo(p) => PathCommandExt::MoveTo(p),
            SvgEvent::RelativeMoveTo(v) => PathCommandExt::RelativeMoveTo(v),
            SvgEvent::LineTo(p) => PathCommandExt::LineTo(p),
            SvgEvent::RelativeLineTo(v) => PathCommandExt::RelativeLineTo(v),
            SvgEvent::CubicTo(c1, c2, p) => PathCommandExt::CubicCurveTo(c1, c2, p),
            SvgEvent::RelativeCubicTo(c1, c2, v) => PathCommandExt::RelativeCubicCurveTo(c1, c2, v),
            SvgEvent::QuadraticTo(c, p) => PathCommandExt::QuadraticCurveTo(c, p),
            SvgEvent::RelativeQuadraticTo(c, v) => PathCommandExt::RelativeQuadraticCurveTo(c, v),
            SvgEvent::SmoothCubicTo(c, p) => PathCommandExt::SmoothCubicCurveTo(c, p),
            SvgEvent::SmoothRelativeCubicTo(c, v) => {
                PathCommandExt::RelativeSmoothCubicCurveTo(c, v)
            }
            SvgEvent::SmoothQuadraticTo(p) => PathCommandExt::SmoothQuadraticCurveTo(p),
            SvgEvent::SmoothRelativeQuadraticTo(v) => {
                PathCommandExt::RelativeSmoothQuadraticCurveTo(v)
            }
            SvgEvent::ArcTo(..) => unimplemented!(),
            SvgEvent::RelativeArcTo(..) => unimplemented!(),
            SvgEvent::HorizontalLineTo(d) => PathCommandExt::HorizontalLineTo(d),
            SvgEvent::RelativeHorizontalLineTo(d) => PathCommandExt::RelativeHorizontalLineTo(d),
            SvgEvent::VerticalLineTo(d) => PathCommandExt::VerticalLineTo(d),
            SvgEvent::RelativeVerticalLineTo(d) => PathCommandExt::RelativeVerticalLineTo(d),
            SvgEvent::Close => PathCommandExt::Close,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub(crate) struct Vertex {
    pub position: [f32; 2],
    pub uv: [f32; 2],
}

/// A tessellated path (consisting of triangles).
#[derive(Debug, Clone)]
pub struct TessellatedPath {
    pub(crate) fill: Option<TessellatedPathPart>,
    pub(crate) stroke: Option<TessellatedPathPart>,
}

#[derive(Debug, Clone)]
pub(crate) struct TessellatedPathPart {
    pub(crate) vertices: Vec<Vertex>,
    pub(crate) indices: Vec<u32>,
    pub(crate) bounding_box: Rect,
}

/// A text.
pub struct Text<'a> {
    /// The bottom-left corner of the text.
    pub origin: Point2D,

    /// The text itself.
    pub string: &'a str,

    /// x size of the text. For best experience, should match `scale_y`.
    pub scale_x: Scalar,

    /// y size of the text. For best experience, should match `scale_x`.
    pub scale_y: Scalar,
}

/// Text constructor.
pub fn text<'a>(x: Scalar, y: Scalar, s: Scalar, string: &'a str) -> Text<'a> {
    Text {
        string,
        origin: Point2D::new(x, y),
        scale_x: s,
        scale_y: s,
    }
}
