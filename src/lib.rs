#[macro_use]
extern crate gfx;
#[macro_use]
extern crate lazy_static;
extern crate euclid;
extern crate font_loader;
extern crate image;
extern crate lyon;
extern crate rusttype;
extern crate unicode_normalization;

mod extra;

pub mod backend;
pub mod geometry;
pub mod object;
pub mod style;

use image::DynamicImage;

use font_loader::system_fonts::{self, FontPropertyBuilder};

use backend::{Backend, Font, Image};
use geometry::{Scalar, Transform2D, Vector2D};
use style::{Color, Effect, Fill, Stroke};

/// The ohm instance, used to draw 2D graphics.
pub struct Ohm<B: Backend> {
    /// The backend.
    pub backend: B,
    stack: Vec<Transform2D>,
    fill: Fill<B>,
    stroke: Stroke,
    font: B::Font,
}

impl<B: Backend> Ohm<B> {
    /// Creates a new instance with the specified backend.
    pub fn new(mut backend: B) -> Ohm<B> {
        let default_font = FontQuery::new().get(&mut backend);

        Ohm {
            backend,
            stack: vec![Transform2D::identity()],
            fill: Default::default(),
            stroke: Default::default(),
            font: default_font,
        }
    }

    /// Presents a frame. Should be called after other drawing operations.
    pub fn commit(&mut self) {
        self.backend.commit();
        self.backend.clear_scissor();
        self.stack.clear();
        self.stack.push(Transform2D::identity());
    }

    /// Updates the hidpi factor of the backend.
    pub fn set_hidpi_factor(&mut self, factor: f32) {
        self.backend.set_hidpi_factor(factor);
    }

    /// The current hidpi factor.
    pub fn get_hidpi_factor(&self) -> f32 {
        self.backend.get_hidpi_factor()
    }

    /// Creates a new image from the specified data.
    pub fn new_image(&mut self, data: &DynamicImage) -> B::Image {
        B::Image::new(&mut self.backend, data)
    }

    /// Creates a new font from the truetype bytes. `index` is the position of the font
    /// in the collection, defaulting to the first available font.
    pub fn new_font(&mut self, bytes: Vec<u8>, index: Option<usize>) -> B::Font {
        B::Font::from_bytes(&mut self.backend, bytes, index)
    }

    /// Sets object fill to a solid color.
    pub fn fill_color<C: Into<Color>>(&mut self, color: C) {
        self.fill.color = color.into();
    }

    /// Sets object fill to transparent.
    pub fn fill_empty(&mut self) {
        self.fill.color.a = 0.0;
    }

    /// Sets object fill to an image.
    pub fn fill_image(&mut self, image: B::Image) {
        self.fill.image = Some(image);
    }

    /// Removes fill image.
    pub fn fill_no_image(&mut self) {
        self.fill.image = None;
    }

    /// Sets the font used in text drawing.
    pub fn font(&mut self, font: B::Font) {
        self.font = font;
    }

    /// Sets object stroke to a solid color.
    pub fn stroke_color<C: Into<Color>>(&mut self, color: C) {
        self.stroke.color = color.into();
    }

    /// Sets stroke thickness.
    pub fn stroke_thickness(&mut self, thickness: f32) {
        self.stroke.thickness = thickness;
    }

    /// Removes the stroke.
    pub fn stroke_empty(&mut self) {
        self.stroke.thickness = 0.0;
    }

    /// Sets the scissor rectangle.
    pub fn set_scissor(&mut self, rect: geometry::Rect) {
        self.backend.set_scissor(rect);
    }

    /// Clears the scissor rectangle.
    pub fn clear_scissor(&mut self) {
        self.backend.clear_scissor();
    }

    /// Sets the effect, applying to the subsequent drawing operations.
    pub fn set_effect(&mut self, effect: Effect) {
        self.backend.set_effect(effect);
    }

    /// Removes the effect.
    ///
    /// This is identical to calling [`set_effect`](Ohm::set_effect)
    /// with [`Effect::Identity`](Effect::Identity).
    pub fn clear_effect(&mut self) {
        self.backend.set_effect(Effect::Identity);
    }

    /// Clears the entire frame with a solid color.
    pub fn clear<C: Into<Color>>(&mut self, color: C) {
        self.backend.clear(color.into());
    }

    /// Draws a rectangle.
    pub fn rect(&mut self, rect: geometry::Rect) {
        let state = self.state();
        self.backend.rect(state, rect);
    }

    /// Draws a circle.
    pub fn circle(&mut self, circle: object::Circle) {
        let state = self.state();
        self.backend.circle(state, circle);
    }

    /// Draws an ellipse.
    pub fn ellipse(&mut self, ellipse: object::Ellipse) {
        let state = self.state();
        self.backend.ellipse(state, ellipse);
    }

    /// Draws a path.
    pub fn path(&mut self, path: object::Path) {
        let state = self.state();
        self.backend.path(state, path)
    }

    /// Draws a text.
    pub fn text(&mut self, text: object::Text) {
        let state = self.state();
        self.backend.text(state, text)
    }

    /// Returns the current transformation matrix.
    pub fn transform(&self) -> Transform2D {
        let l = self.stack.len();
        self.stack[l - 1]
    }

    /// Returns a mutable reference to the current transformation matrix.
    pub fn transform_mut(&mut self) -> &mut Transform2D {
        let l = self.stack.len();
        &mut self.stack[l - 1]
    }

    /// Translates the coordinate system by a vector.
    pub fn translate(&mut self, vector: Vector2D) {
        let t = self.transform_mut();
        *t = t.post_translate(vector);
    }

    /// Scales the coordinate system.
    pub fn scale(&mut self, x: Scalar, y: Scalar) {
        let t = self.transform_mut();
        *t = t.post_scale(x, y)
    }

    /// Rotates the coordinate system.
    pub fn rotate(&mut self, angle: Scalar) {
        let t = self.transform_mut();
        *t = t.post_rotate(euclid::Angle::radians(angle))
    }

    /// Pushes the current transformation matrix onto the stack.
    pub fn push_transform(&mut self) {
        let t = self.transform();
        self.stack.push(t);
    }

    /// Pops the top transformation matrix from the stack.
    ///
    /// # Panics
    ///
    /// Panics when popping the root matrix.
    pub fn pop_transform(&mut self) {
        if self.stack.len() == 1 {
            panic!("Tried to pop root transform matrix");
        }

        self.stack.pop();
    }

    /// Returns the current graphics state.
    pub fn state(&self) -> State<B> {
        State {
            transform: self.transform(),
            fill: self.fill.clone(),
            stroke: self.stroke,
            font: self.font.clone(),
        }
    }
}

/// Font query options.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct FontQuery<'a> {
    italic: bool,
    oblique: bool,
    bold: bool,
    monospace: bool,
    family: Option<&'a str>,
}

impl<'a> FontQuery<'a> {
    /// Creates a new font query with no filters.
    pub fn new() -> FontQuery<'a> {
        FontQuery {
            italic: false,
            oblique: false,
            bold: false,
            monospace: false,
            family: None,
        }
    }

    /// Search only for italic fonts.
    pub fn italic(mut self, italic: bool) -> FontQuery<'a> {
        self.italic = italic;
        self
    }

    /// Search only for oblique fonts.
    pub fn oblique(mut self, oblique: bool) -> FontQuery<'a> {
        self.oblique = oblique;
        self
    }

    /// Search only for bold fonts.
    pub fn bold(mut self, bold: bool) -> FontQuery<'a> {
        self.bold = bold;
        self
    }

    /// Search only for monospace fonts.
    pub fn monospace(mut self, monospace: bool) -> FontQuery<'a> {
        self.monospace = monospace;
        self
    }

    /// Search only for fonts matching the pattern.
    pub fn family(mut self, family: &'a str) -> FontQuery<'a> {
        self.family = Some(family);
        self
    }

    /// Returns the first appropriate font.
    pub fn get<B: Backend>(&self, back: &mut B) -> B::Font {
        let mut b = FontPropertyBuilder::new();

        if self.italic {
            b = b.italic();
        }

        if self.oblique {
            b = b.oblique();
        }

        if self.bold {
            b = b.bold();
        }

        if self.monospace {
            b = b.monospace();
        }

        if let Some(family) = self.family {
            b = b.family(family);
        }

        let prop = b.build();

        let (data, index) = system_fonts::get(&prop).unwrap();
        B::Font::from_bytes(back, data, Some(index as usize))
    }
}

/// The graphics state.
pub struct State<B: Backend> {
    /// Current transformation matrix.
    pub transform: Transform2D,

    /// Object fill.
    pub fill: Fill<B>,

    /// Object stroke.
    pub stroke: Stroke,

    /// Text font.
    pub font: B::Font,
}

impl<B: Backend> State<B> {
    /// Returns `true` if the fill is not transparent.
    pub fn has_fill(&self) -> bool {
        self.fill.color.a >= 0.0
    }

    /// Returns `true` if the objects should have a stroke.
    pub fn has_stroke(&self) -> bool {
        !self.stroke.is_empty()
    }

    /// Returns `true` if the object is opaque.
    pub fn is_opaque(&self) -> bool {
        if self.fill.color.a > 0.0 && self.fill.color.a < 1.0 {
            return false;
        }

        if self.has_stroke() && self.stroke.color.a < 1.0 {
            return false;
        }

        true
    }
}

impl<B: Backend> Clone for State<B> {
    fn clone(&self) -> State<B> {
        State {
            transform: self.transform,
            fill: self.fill.clone(),
            stroke: self.stroke,
            font: self.font.clone(),
        }
    }
}
