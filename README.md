# Ohm 2D rendering engine

This is backend-agnostic vector graphics rendering engine, primarily focused on
creating fast hardware-accelerated user interfaces.

Currently there are only [gfx-rs](https://github.com/gfx-rs/gfx/tree/pre-ll)
(notice the `pre-ll` branch) backend implemented. A software-rendering
backend is not yet planned, though it could be added in future.

The goal is to replace cairo (in which OpenGL support is still
very experemental) and QPainter (which still does not use OpenGL backend
for widgets) with a safe Rust alternative suitable for browsers and
GUI toolkits.

## Status

This library is a work in progress. API is very unstable.

## Documentation

 - [master branch](https://leshainc.gitlab.io/ohm)

## Features

 - [x] Drawing simple shapes with instancing to reduce GPU uploads.
 - [x] Drawing paths with automatic batching (tessellation is implemented
       in the [lyon](https://github.com/nical/lyon) crate).
 - [x] Rendering text using the [rusttype](https://gitlab.redox-os.org/redox-os/rusttype) crate with a GPU cache.
 - [x] Proper transparency, scissor test, images as background.
 - [ ] Effect stack with support of real-time blur and shadows. (Currently WIP:
       blur is already supported).

## Non-goals

 - Games. See [piston](http://www.piston.rs/), [amethyst](https://www.amethyst.rs/),
   and the http://arewegameyet.com/ website.
   This library was not designed for games, though you are free to use it
   for them if you think ohm is sufficient for you needs.

## Example

This library provides a simple immediate-mode drawing API.

```rust
extern crate ohm;

// ...other imports, e.g. a window handling library, backend specific crates

use ohm::geometry::{Point2D, Vector2D};
use ohm::{object, Ohm};

// ...create backend

let mut ohm = Ohm::new(backend);

loop {
    // ...handle window events

    ohm.clear([1.0, 1.0, 1.0]);

    ohm.stroke_color([0.1, 0.1, 0.1]);
    ohm.stroke_thickness(2.0);

    ohm.fill_color([1.0, 0.0, 0.0, 0.5]);
    ohm.rect(object::rect(75.0, 150.0, 300.0, 300.0));

    ohm.fill_color([1.0, 1.0, 1.0, 0.8]);
    ohm.circle(object::circle(400.0, 300.0, 200.0));

    ohm.fill_color([0.0, 0.0, 1.0, 0.5]);
    ohm.rect(object::rect(425.0, 150.0, 300.0, 300.0));

    ohm.push_transform();
    ohm.fill_color([1.0, 1.0, 1.0, 0.5]);
    ohm.ellipse(object::ellipse(400.0, 300.0, 300.0, 100.0));
    ohm.pop_transform();

    ohm.stroke_color([0.8, 0.1, 0.1]);
    ohm.fill_color([0.0, 0.0, 0.0, 0.0]);
    ohm.stroke_thickness(6.0);

    let path = object::Path::new_rust_logo();

    ohm.fill_color([0.0, 0.0, 0.0]);
    ohm.stroke_color([0.1, 0.1, 0.1]);
    ohm.stroke_thickness(0.5);
    ohm.push_transform();
    ohm.translate(Vector2D::new(520.0, 350.0));
    ohm.scale(1.2, 1.2);
    ohm.path(path);
    ohm.pop_transform();

    ohm.fill_color([0.0, 0.0, 0.0]);
    ohm.text(object::text(50.0, 550.0, 35.0, "Hello, World!"));

    ohm.commit();

    // ...backend-specific flush
}
```

For real examples see the `examples/` folder.

To run an example execute the following command in the project directory.

```
$ cargo run --example <name>
```

Note that the release mode may greatly increase the performace in heavy
examples, like `psycho`.

```
$ cargo run --release --example <name>
```

## License

This project is licensed like the Rust language itself under either of

 - Apache License, Version 2.0 (see the `LICENSE-APACHE` file
   or http://www.apache.org/licenses/LICENSE-2.0)
 - MIT license (see the `LICENSE-MIT` file
   or https://opensource.org/licenses/MIT)

at your option.
