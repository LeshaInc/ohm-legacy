#version 150 core

in vec2 inPos;
in vec2 inUV;
in vec4 inColor;

in float inZ;

out vec4 fColor;
out vec2 fUV;

uniform mat4 uProj;

void main() {
    fColor = inColor;
    fUV = inUV;

    gl_Position = uProj * vec4(inPos, inZ, 1.0);
}
