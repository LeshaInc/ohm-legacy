#version 150 core

in vec2 inPos;
in vec2 inUV;

in vec4 inTransform0;
in vec4 inTransform1;
in vec4 inTransform2;

in vec2 inSize;
in float inZ;

in vec4 inFillColor;

out vec4 fFillColor;
out vec2 fUV;

uniform mat4 uProj;

void main() {
    fFillColor = inFillColor;
    fUV = inUV;

    mat4 model = transpose(mat4(inTransform0, inTransform1, inTransform2, vec4(0.0, 0.0, 0.0, 1.0)));
    gl_Position = uProj * model * vec4(inPos * inSize, inZ, 1.0);
}
