#version 150 core

in vec2 inPos;
in vec2 inUV;

out vec2 fUV;

uniform float uZ;

void main() {
    fUV = inUV;

    gl_Position = vec4(inPos, uZ, 1.0);
}
