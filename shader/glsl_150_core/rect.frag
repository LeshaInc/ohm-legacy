#version 150 core

in vec4 fFillColor;
in vec2 fUV;

out vec4 outColor;

uniform sampler2D uTexture;

void main() {
    outColor = texture(uTexture, fUV) * fFillColor;
}
