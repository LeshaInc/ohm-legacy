#version 150 core

in vec4 fFillColor;
in vec2 fUV;

out vec4 outColor;

uniform sampler2D uCache;
uniform float uAlphaMultiplier;

void main() {
    float a = texture(uCache, fUV).r;

    if (a <= 0.0)
        discard;

    a = clamp(a * uAlphaMultiplier, 0.0, 1.0);
    outColor = fFillColor * vec4(1.0, 1.0, 1.0, a);
}
