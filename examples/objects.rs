extern crate ohm;

extern crate gfx;
extern crate gfx_window_glutin;
extern crate glutin;

extern crate find_folder;
extern crate image;

use std::f32::consts::PI;
use std::time::{Duration, Instant};

use ohm::backend::gfx::{ColorFormat, DepthFormat, GfxBackend};
use ohm::backend::Backend;
use ohm::geometry::{Point2D, Vector2D};
use ohm::{object, style, Ohm};

use gfx::Device;
use glutin::{Api, Event, GlContext, GlRequest, WindowEvent};

const GL_REQUEST: GlRequest = GlRequest::Specific(Api::OpenGl, (3, 2));

fn main() {
    let mut events_loop = glutin::EventsLoop::new();

    let window_builder = glutin::WindowBuilder::new()
        .with_title("Ohm 2D")
        .with_dimensions((800, 600).into())
        .with_min_dimensions((800, 600).into())
        .with_max_dimensions((800, 600).into())
        .with_resizable(false);

    let context_builder = glutin::ContextBuilder::new()
        .with_gl(GL_REQUEST)
        .with_vsync(true)
        .with_multisampling(16)
        .with_srgb(true);

    let (window, mut device, mut factory, mut color_target, mut depth_target) =
        gfx_window_glutin::init::<ColorFormat, DepthFormat>(
            window_builder,
            context_builder,
            &events_loop,
        );

    let encoder = factory.create_command_buffer().into();

    let backend = GfxBackend::build()
        .with_factory(factory)
        .with_encoder(encoder)
        .with_targets(color_target.clone(), depth_target.clone())
        .with_dimensions((800.0, 600.0))
        .with_hidpi_factor(window.get_hidpi_factor() as f32)
        .build();

    let mut ohm = Ohm::new(backend);
    let mut running = true;

    let mut frames = 0;
    let start = Instant::now();
    let mut counter_start = Instant::now();

    let mut root = find_folder::Search::ParentsThenKids(3, 3)
        .for_folder("examples")
        .unwrap();
    root.push("lol.png");

    let data = image::open(&root).unwrap();
    let lol_image = ohm.new_image(&data);

    root.pop();
    root.push("grid.jpg");

    let data = image::open(&root).unwrap();
    let grid_image = ohm.new_image(&data);

    while running {
        events_loop.poll_events(|event| {
            if let Event::WindowEvent { event, .. } = event {
                match event {
                    WindowEvent::CloseRequested => running = false,
                    WindowEvent::Resized(size) => {
                        println!("resize {:?}", size);
                        window.resize(size.to_physical(window.get_hidpi_factor()));
                        gfx_window_glutin::update_views(
                            &window,
                            &mut color_target,
                            &mut depth_target,
                        );
                        ohm.backend
                            .update_views(color_target.clone(), depth_target.clone());
                        ohm.backend.resize((size.width as f32, size.height as f32));
                    }
                    _ => {}
                }
            }
        });

        let duration = start.elapsed();
        let t = duration.as_secs() as f32 + (duration.subsec_nanos() as f32) / 1_000_000_000.0;
        let s = ((t * 10.0).sin() + 1.0) * 10.0 + 1.0;
        let r = t % 1.0 * PI;

        ohm.clear([1.0, 1.0, 1.0]);

        ohm.stroke_color([0.1, 0.1, 0.1]);
        ohm.stroke_thickness(s);

        ohm.fill_color([1.0, 0.0, 0.0, 0.5]);
        ohm.fill_image(grid_image.clone());
        ohm.rect(object::rect(75.0, 150.0, 300.0, 300.0));

        ohm.stroke_thickness(0.0);
        ohm.fill_color([1.0, 1.0, 1.0, 0.8]);
        ohm.fill_image(lol_image.clone());
        ohm.circle(object::circle(400.0, 300.0, 200.0));

        ohm.stroke_thickness(s);
        ohm.fill_color([0.0, 0.0, 1.0, 0.5]);
        ohm.fill_image(grid_image.clone());
        ohm.rect(object::rect(425.0, 150.0, 300.0, 300.0));

        ohm.push_transform();
        ohm.translate(Vector2D::new(-400.0, -300.0));
        ohm.rotate(r);
        ohm.translate(Vector2D::new(400.0, 300.0));
        ohm.fill_color([1.0, 1.0, 1.0, 0.5]);
        ohm.fill_no_image();
        ohm.ellipse(object::ellipse(400.0, 300.0, 300.0, 100.0));
        ohm.pop_transform();

        ohm.stroke_color([0.8, 0.1, 0.1]);
        ohm.fill_color([0.0, 0.0, 0.0, 0.0]);
        ohm.stroke_thickness(6.0);

        let mut path = object::Path::new();
        path.move_to(Point2D::new(105.0, 0.0));
        path.arc(Point2D::new(0.0, 0.0), 105.0, 105.0, 2.0 * PI, 0.0);
        path.close();

        path.move_to(Point2D::new(0.0, 100.0));

        for i in 0..5 {
            let (sin, cos) = (2.0 * PI / 5.0 * ((i * 2) as f32 - 1.25)).sin_cos();
            path.line_to(Point2D::new(100.0 * cos, -100.0 * sin));
        }

        path.close();

        ohm.push_transform();
        ohm.translate(Vector2D::new(115.0, 115.0));
        ohm.path(path);
        ohm.pop_transform();

        let path = object::Path::new_rust_logo();

        let s = ((t * 10.0).sin() + 1.0) * 1.5;

        ohm.set_effect(style::Effect::GaussianBlur(8.0));

        ohm.fill_color([1.0, 1.0, 1.0]);
        ohm.fill_image(grid_image.clone());
        ohm.stroke_color([0.1, 0.1, 0.1]);
        ohm.stroke_thickness(s);
        ohm.push_transform();
        ohm.translate(Vector2D::new(520.0, 350.0));
        ohm.scale(1.2, 1.2);
        ohm.path(path);
        ohm.fill_no_image();
        ohm.pop_transform();

        ohm.clear_effect();

        ohm.fill_color([0.0, 0.0, 0.0]);
        ohm.text(object::text(50.0, 550.0, 35.0, "Hello, World!"));

        ohm.commit();
        ohm.backend.encoder.flush(&mut device);
        window.swap_buffers().unwrap();
        device.cleanup();

        frames += 1;

        if counter_start.elapsed() >= Duration::from_secs(5) {
            let duration = counter_start.elapsed();
            counter_start = Instant::now();

            let t = duration.as_secs() as f32 + (duration.subsec_nanos() as f32) / 1_000_000_000.0;
            let t = t / (frames as f32);
            println!("{:.5} s; {:.5} fps", t, 1.0 / t);

            frames = 0;
        }
    }
}
