extern crate ohm;

extern crate gfx;
extern crate gfx_window_glutin;
extern crate glutin;

use std::time::{Duration, Instant};

use ohm::backend::gfx::{ColorFormat, DepthFormat, GfxBackend};
use ohm::backend::Backend;
use ohm::{object, Ohm};

use gfx::Device;
use glutin::{Api, Event, GlContext, GlRequest, WindowEvent};

use std::alloc::System;

#[global_allocator]
static GLOBAL: System = System;

const GL_REQUEST: GlRequest = GlRequest::Specific(Api::OpenGl, (3, 2));

fn main() {
    let mut events_loop = glutin::EventsLoop::new();

    let window_builder = glutin::WindowBuilder::new()
        .with_title("Ohm 2D")
        .with_dimensions((800, 600).into())
        .with_min_dimensions((800, 600).into())
        .with_max_dimensions((800, 600).into())
        .with_resizable(false);

    let context_builder = glutin::ContextBuilder::new()
        .with_gl(GL_REQUEST)
        .with_vsync(true)
        .with_multisampling(16)
        .with_srgb(true);

    let (window, mut device, mut factory, mut color_target, mut depth_target) =
        gfx_window_glutin::init::<ColorFormat, DepthFormat>(
            window_builder,
            context_builder,
            &events_loop,
        );

    let encoder = factory.create_command_buffer().into();

    let backend = GfxBackend::build()
        .with_factory(factory)
        .with_encoder(encoder)
        .with_targets(color_target.clone(), depth_target.clone())
        .with_dimensions((800.0, 600.0))
        .with_hidpi_factor(window.get_hidpi_factor() as f32)
        .build();

    let mut ohm = Ohm::new(backend);
    let mut running = true;

    let mut frames = 0;
    let mut counter_start = Instant::now();

    while running {
        events_loop.poll_events(|event| {
            if let Event::WindowEvent { event, .. } = event {
                match event {
                    WindowEvent::CloseRequested => running = false,
                    WindowEvent::Resized(size) => {
                        println!("resize {:?}", size);
                        window.resize(size.to_physical(window.get_hidpi_factor()));
                        gfx_window_glutin::update_views(
                            &window,
                            &mut color_target,
                            &mut depth_target,
                        );
                        ohm.backend
                            .update_views(color_target.clone(), depth_target.clone());
                        ohm.backend.resize((size.width as f32, size.height as f32));
                    }
                    _ => {}
                }
            }
        });

        ohm.clear([1.0, 1.0, 1.0]);

        ohm.rect(object::rect(0.0, 0.0, 800.0, 50.0));
        ohm.set_scissor(object::rect(100.0, 100.0, 600.0, 300.0));
        ohm.rect(object::rect(0.0, 0.0, 800.0, 600.0));

        ohm.commit();
        ohm.backend.encoder.flush(&mut device);
        window.swap_buffers().unwrap();
        device.cleanup();

        frames += 1;

        if counter_start.elapsed() >= Duration::from_secs(5) {
            let duration = counter_start.elapsed();
            counter_start = Instant::now();

            let t = duration.as_secs() as f32 + (duration.subsec_nanos() as f32) / 1_000_000_000.0;
            let t = t / (frames as f32);
            println!("{:.5} s; {:.5} fps", t, 1.0 / t);

            frames = 0;
        }
    }
}
